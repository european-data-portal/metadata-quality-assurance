package de.fhg.fokus.edp.mqa.service.test;

import de.fhg.fokus.edp.mqa.service.model.Licence;
import de.fhg.fokus.edp.mqa.service.test.util.ArquillianUtil;
import de.fhg.fokus.edp.mqa.service.test.util.EntityUtils;
import de.fhg.fokus.edp.mqa.service.test.util.TestConstants;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.inject.Inject;

/**
 * Created by fritz on 15.03.17.
 */
public class LicenceDeletionTest extends Arquillian {

    private static Logger LOG = LoggerFactory.getLogger(LicenceDeletionTest.class);

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient dc;

    @Inject
    private EntityUtils utils;

    /**
     * Create deployment web archive.
     *
     * @return the web archive
     */
    @Deployment
    public static WebArchive createDeployment() {
        WebArchive war = ArquillianUtil.createWebArchive();
        LOG.info(war.toString(true));
        return war;
    }

    @Test
    public void deleteLicences() {
        utils.init();
        Licence l = dc.getLicenceById(TestConstants.LICENCE_NAME_KNOWN);
        Assert.assertNotNull(l);
        int count = dc.countAllLicences();
        dc.removeEntity(l);
        Assert.assertTrue(count > dc.countAllLicences());
    }
}

package de.fhg.fokus.edp.mqa.service.test.util;

import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.*;
import de.fhg.fokus.edp.mqa.service.persistence.entity.*;
import de.fhg.fokus.edp.mqa.service.utils.Formats;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.slf4j.Logger;
import org.testng.Assert;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by bdi on 05/08/15.
 */
@RequestScoped
public class EntityUtils {

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    private Formats formats;

    @Inject
    @Log
    Logger logger;

    private Catalog generateTestCatalog(String name) {
        Catalog catalog = new CatalogEntity();
        catalog.setName(name);
        catalog.setTitle(String.format("%s-title", name));
        catalog.setInstanceId(name + "_id");

        return catalog;
    }

    private Publisher generateTestPublisher() {
        Publisher publisher = new PublisherEntity();
        publisher.setName("Hans Müller");
        publisher.setEmail("hans.müller@mueller.de");

        return publisher;
    }

    private Dataset generateTestDataset(String name) {
        Dataset dataset = new DatasetEntity();
        dataset.setName(name);
        dataset.setTitle(name);
        dataset.setInstanceId(UUID.randomUUID().toString());

        return dataset;
    }

    private Violation generateTestViolation() {
        Violation violation = new ViolationEntity();
        violation.setViolationName(TestConstants.VIOLATION_NAME);
        violation.setViolationInstance("<Mandatory field>");

        return violation;
    }

    private Licence generateTestLicence(String name) {
        Licence licence = new LicenceEntity();
        licence.setLicenceId(name);
        return licence;
    }

    /**
     * Init.
     */
    public void init() {

        Assert.assertNotNull(vdc);

        // CATALOG
        Catalog catalogNotOk = generateTestCatalog(TestConstants.CATALOG_NAME_NOT_OK);
        vdc.persistEntity(catalogNotOk);
        Assert.assertEquals(vdc.getCatalogByName(catalogNotOk.getName()).getName(), catalogNotOk.getName());

        Catalog catalogOk = generateTestCatalog(TestConstants.CATALOG_NAME_OK);
        vdc.persistEntity(catalogOk);
        Assert.assertEquals(vdc.getCatalogByName(catalogOk.getName()).getName(), catalogOk.getName());

        // PUBLISHER
        Publisher publisher = generateTestPublisher();
        vdc.persistEntity(publisher);
        catalogNotOk.setPublisher(publisher);
        catalogOk.setPublisher(publisher); // TODO: create own publisher
        vdc.updateEntity(catalogNotOk);
        vdc.updateEntity(catalogOk);
        Assert.assertEquals(vdc.getCatalogByName(catalogNotOk.getName()).getPublisher().getName(), publisher.getName());

        // DATASET
        Dataset datasetOk = generateTestDataset(TestConstants.DATASET_OK);
        Dataset datasetNotOk = generateTestDataset(TestConstants.DATASET_NOT_OK);
        Dataset datasetOkForCOk = generateTestDataset(TestConstants.DATASET_OK_FOR_CATALOG_OK);
        vdc.persistEntity(datasetNotOk);
        vdc.persistEntity(datasetOk);
        vdc.persistEntity(datasetOkForCOk);
        catalogNotOk.getDatasets().add(datasetOk);
        catalogNotOk.getDatasets().add(datasetNotOk);
        catalogOk.getDatasets().add(datasetOkForCOk);
        vdc.updateEntity(catalogNotOk);
        vdc.updateEntity(catalogOk);

        Assert.assertEquals(vdc.getDatasetByName(datasetOk.getName()).getName(), datasetOk.getName());
        Assert.assertEquals(vdc.getDatasetByName(datasetNotOk.getName()).getName(), datasetNotOk.getName());

        // VIOLATION
        Violation violation = generateTestViolation();
        vdc.persistEntity(violation);
        datasetNotOk.getViolations().add(violation);
        vdc.updateEntity(datasetNotOk);

        // LICENCE
        Licence licence = generateTestLicence(TestConstants.LICENCE_NAME_KNOWN);
        vdc.persistEntity(licence);
        licence = generateTestLicence("licence-unrelated");
        vdc.persistEntity(licence);
        datasetOk.setLicenceId(TestConstants.LICENCE_NAME_KNOWN);
        datasetNotOk.setLicenceId(TestConstants.LICENCE_NAME_UNKNOWN);
        logger.info("Added licences to dataset-ok and dataset-not-ok");
        vdc.updateEntity(datasetOk);
        vdc.updateEntity(datasetNotOk);

        // DISTRIBUTION
        Distribution distributionOk = new DistributionEntity();
        distributionOk.setAccessUrl("https://www.tes.t");
        distributionOk.setDownloadUrl("https://www.tes.t");
        distributionOk.setStatusAccessUrl(200);
        distributionOk.setStatusDownloadUrl(200);
        distributionOk.setFormat("csV");
        distributionOk.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionOk.setStatusDownloadUrlLastChangeDate(LocalDateTime.now());
        distributionOk.setInstanceId(UUID.randomUUID().toString());

        Distribution distributionTsv = new DistributionEntity();
        distributionTsv.setAccessUrl("https://www.tes.t");
        distributionTsv.setDownloadUrl("https://www.tes.t");
        distributionTsv.setStatusAccessUrl(200);
        distributionTsv.setStatusDownloadUrl(200);
        distributionTsv.setFormat("TSV");
        distributionTsv.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionTsv.setStatusDownloadUrlLastChangeDate(LocalDateTime.now());
        distributionTsv.setInstanceId(UUID.randomUUID().toString());

        Distribution distributionCsv = new DistributionEntity();
        distributionCsv.setAccessUrl("https://www.tes.t");
        distributionCsv.setDownloadUrl("https://www.tes.t");
        distributionCsv.setStatusAccessUrl(200);
        distributionCsv.setStatusDownloadUrl(200);
        distributionCsv.setFormat("csv");
        distributionCsv.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionCsv.setStatusDownloadUrlLastChangeDate(LocalDateTime.now());
        distributionCsv.setInstanceId(UUID.randomUUID().toString());

        Distribution distributionAccessNotOkDownloadOk = new DistributionEntity();
        distributionAccessNotOkDownloadOk.setAccessUrl("https://www.tes.t");
        distributionAccessNotOkDownloadOk.setDownloadUrl("https://www.tes.t");
        distributionAccessNotOkDownloadOk.setStatusAccessUrl(408);
        distributionAccessNotOkDownloadOk.setAccessErrorMessage("Timeout");
        distributionAccessNotOkDownloadOk.setStatusDownloadUrl(200);
        distributionAccessNotOkDownloadOk.setFormat("docx");
        distributionAccessNotOkDownloadOk.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionAccessNotOkDownloadOk.setStatusDownloadUrlLastChangeDate(LocalDateTime.now());
        distributionAccessNotOkDownloadOk.setInstanceId(UUID.randomUUID().toString());

        Distribution distributionDocx = new DistributionEntity();
        distributionDocx.setAccessUrl("https://www.tes.t");
        distributionDocx.setDownloadUrl("https://www.tes.t");
        distributionDocx.setStatusAccessUrl(408);
        distributionDocx.setAccessErrorMessage("Timeout");
        distributionDocx.setStatusDownloadUrl(200);
        distributionDocx.setFormat("docx");
        distributionDocx.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionDocx.setStatusDownloadUrlLastChangeDate(LocalDateTime.now());
        distributionDocx.setInstanceId(UUID.randomUUID().toString());

        Distribution distributionAccessOkDownloadNotOk = new DistributionEntity();
        distributionAccessOkDownloadNotOk.setAccessUrl("https://www.tes.t");
        distributionAccessOkDownloadNotOk.setDownloadUrl("https://www.tes.t");
        distributionAccessOkDownloadNotOk.setStatusAccessUrl(200);
        distributionAccessOkDownloadNotOk.setStatusDownloadUrl(408);
        distributionAccessOkDownloadNotOk.setDownloadErrorMessage("Timeout");
        distributionAccessOkDownloadNotOk.setFormat("CSV");
        distributionAccessOkDownloadNotOk.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionAccessOkDownloadNotOk.setStatusDownloadUrlLastChangeDate(LocalDateTime.now());
        distributionAccessOkDownloadNotOk.setInstanceId(UUID.randomUUID().toString());

        Distribution distributionAccessOkDownloadNull = new DistributionEntity();
        distributionAccessOkDownloadNull.setAccessUrl("https://www.tes.t");
        distributionAccessOkDownloadNull.setStatusAccessUrl(200);
        distributionAccessOkDownloadNull.setFormat("DocX");
        distributionAccessOkDownloadNull.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionAccessOkDownloadNull.setInstanceId(UUID.randomUUID().toString());

        Distribution distributionDatasetOkForCOK = new DistributionEntity();
        distributionDatasetOkForCOK.setAccessUrl("https://www.tes.t");
        distributionDatasetOkForCOK.setStatusAccessUrl(404);
        distributionDatasetOkForCOK.setFormat("PDF");
        distributionDatasetOkForCOK.setStatusAccessUrlLastChangeDate(LocalDateTime.now());
        distributionDatasetOkForCOK.setInstanceId(UUID.randomUUID().toString());

        vdc.persistEntity(distributionAccessNotOkDownloadOk);
        vdc.persistEntity(distributionAccessOkDownloadNotOk);
        vdc.persistEntity(distributionOk);
        vdc.persistEntity(distributionAccessOkDownloadNull);
        vdc.persistEntity(distributionCsv);
        vdc.persistEntity(distributionTsv);
        vdc.persistEntity(distributionDocx);
        vdc.persistEntity(distributionDatasetOkForCOK);

        datasetOk.getDistributions().add(distributionOk);
        datasetOk.getDistributions().add(distributionCsv);
        datasetOk.getDistributions().add(distributionTsv);
        datasetNotOk.getDistributions().add(distributionDocx);
        datasetNotOk.getDistributions().add(distributionAccessNotOkDownloadOk);
        datasetNotOk.getDistributions().add(distributionAccessOkDownloadNotOk);
        datasetNotOk.getDistributions().add(distributionAccessOkDownloadNull);
        datasetOkForCOk.getDistributions().add(distributionDatasetOkForCOK);
        logger.info("Added dists to datasets");

        vdc.updateEntity(datasetNotOk);
        vdc.updateEntity(datasetOk);
        vdc.updateEntity(datasetOkForCOk);
        logger.info("Updated datasets with dists and licences");

        Assert.assertFalse(vdc.listAllViolations().isEmpty());

        Assert.assertTrue(vdc.countAllDatasets() == 3);
        Assert.assertEquals(vdc.getDistributionByInstanceId(distributionOk.getInstanceId()).getInstanceId(), distributionOk.getInstanceId());
        Assert.assertEquals(vdc.getDistributionByInstanceId(distributionAccessNotOkDownloadOk.getInstanceId()).getInstanceId(), distributionAccessNotOkDownloadOk
                .getInstanceId());
    }
}

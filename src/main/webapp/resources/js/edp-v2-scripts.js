jQuery(document).ready(function($) {

    $('.navbar-collapse.navbar-ex1-collapse.collapse').on('shown.bs.collapse', function() {
        $(".navbar.navbar-default").addClass("opened-child");
    });
    $('.navbar-collapse.navbar-ex1-collapse.collapse').on('hidden.bs.collapse', function() {
        $(".navbar.navbar-default").removeClass("opened-child");
    });

    // Back on top link
    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.btn-back-top').fadeIn(200);
        } else {
            $('.btn-back-top').fadeOut(200);
        }
    });

    $('.btn-back-top').on("click", function(e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: 0 }, 300);
        $(this).blur();
        return false;
    });

    /* Add padding-top to main panel if second nav is heigher than 52px */
    if ($('#main-menu .navbar-nav > li > .dropdown-menu.active-trail').height() > 52) {
        var dHeight = $('#main-menu .navbar-nav > li > .dropdown-menu.active-trail').height();
        $('#layout-body > .panel-body').css('padding-top', dHeight - 52 + 15);
    };

    /* Secondary nav same height */
    var maxHeight = 0;
    $("#main-menu .navbar-nav > li > .dropdown-menu.active-trail").each(function() {
        if ($(this).outerHeight() > maxHeight) { maxHeight = $(this).outerHeight(); }
    });
    $("#main-menu .navbar-nav > li > .dropdown-menu").css("min-height", maxHeight);

});

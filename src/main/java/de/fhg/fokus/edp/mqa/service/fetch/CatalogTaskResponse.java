package de.fhg.fokus.edp.mqa.service.fetch;

import java.time.Duration;

/**
 * Created by fritz on 14.03.17.
 */
public class CatalogTaskResponse {

    private String catalogName;

    private int datasetCount;

    private Duration duration;

    public CatalogTaskResponse(String catalogName, int datasetCount, Duration duration) {
        this.catalogName = catalogName;
        this.datasetCount = datasetCount;
        this.duration = duration;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public int getDatasetCount() {
        return datasetCount;
    }

    public void setDatasetCount(int datasetCount) {
        this.datasetCount = datasetCount;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}

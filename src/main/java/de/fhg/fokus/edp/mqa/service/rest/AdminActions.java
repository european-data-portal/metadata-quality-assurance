package de.fhg.fokus.edp.mqa.service.rest;

import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapData;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapDataClient;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapDataClientType;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.timer.MqaScheduler;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.SchedulerException;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//TODO extract request body parsing and authentication check
@Path("/admin")
public class AdminActions {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private MqaScheduler mqaScheduler;

    @Inject
    @DcatapData(DcatapDataClientType.CKAN)
    private DcatapDataClient dcatapDataClient;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.ADMIN_TOKEN)
    private String adminToken;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/run")
    public Response triggerValidationRun(String requestBody) {

        if (adminToken != null) {
            try {
                JSONObject json = new JSONObject(requestBody);
                String requestToken = json.getString("secret");

                boolean isAuthValid = !adminToken.isEmpty() && adminToken.equals(requestToken);
                if (isAuthValid) {
                    try {
                        if (mqaScheduler.runValidationJobNow()) {
                            LOG.info("Validation job triggered successfully via API.");
                            return Response.status(204).build();
                        } else {
                            return Response.status(202).build();
                        }
                    } catch (SchedulerException e) {
                        LOG.warn("Requested validation run failed. Returning status 500.", e);
                        return Response.status(500).build();
                    }
                } else {
                    LOG.warn("Requested validation run was not triggered due to failed authentication. Returning status 401.");
                    return Response.status(401).build();
                }
            } catch (JSONException e) {
                LOG.error("Triggering validation run via API failed.", e);
                return Response.status(400).build();
            }
        } else {
            LOG.error("Validation run requested, but no admin token has been configured. Returning status 500.");
            return Response.status(500).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/delete")
    public Response triggerDeletionRun(String requestBody) {

        if (adminToken != null) {
            try {
                JSONObject json = new JSONObject(requestBody);
                String requestToken = json.getString("secret");

                boolean isAuthValid = !adminToken.isEmpty() && adminToken.equals(requestToken);
                if (isAuthValid) {
                    try {
                        if (mqaScheduler.runDeletionJobNow()) {
                            LOG.info("Deletion job triggered successfully via API.");
                            return Response.status(204).build();
                        } else {
                            return Response.status(202).build();
                        }
                    } catch (SchedulerException e) {
                        LOG.warn("Requested deletion run failed. Returning status 500.", e);
                        return Response.status(500).build();
                    }
                } else {
                    LOG.warn("Requested deletion run was not triggered due to failed authentication. Returning status 401.");
                    return Response.status(401).build();
                }
            } catch (JSONException e) {
                LOG.error("Triggering deletion run via API failed.", e);
                return Response.status(400).build();
            }
        } else {
            LOG.error("Deletion run requested, but no admin token has been configured. Returning status 500.");
            return Response.status(500).build();
        }
    }

    @POST
    @Consumes
    @Path("/fingerprint")
    public Response fingerprintLanguages(@QueryParam("languages") @DefaultValue(Constants.LANGUAGE_WILDCARD) String languages, String requestBody) {
        if (adminToken != null && !adminToken.isEmpty()) {
            try {
                JSONObject json = new JSONObject(requestBody);
                String requestToken = json.getString("secret");

                // check for matching credentials
                if (adminToken.equals(requestToken)) {
                    try {
                        if (mqaScheduler.runFingerprintingJobNow(languages))
                            LOG.info("Fingerprinting job for language(s) [{}] successfully requested via API", languages);

                    } catch (SchedulerException e) {
                        LOG.error("Error starting fingerprinting job with identity [{}]", languages, e);
                    }

                    return Response.status(202).build();
                } else {
                    return Response.status(401).build();
                }
            } catch (JSONException e) {
                LOG.error("Triggering fingerprinting of languages [{}] via API failed.", languages, e);
                return Response.status(400).build();
            }
        } else {
            LOG.error("Fingerprinting languages [{}] requested, but no admin token has been configured. Returning status 500.", languages);
            return Response.status(500).build();
        }
    }

    @POST
    @Consumes
    @Path("/refreshLicences")
    public Response refreshLicences(String requestBody) {
        if (adminToken != null && !adminToken.isEmpty()) {
            try {
                JSONObject json = new JSONObject(requestBody);
                String requestToken = json.getString("secret");

                // check for matching credentials
                if (adminToken.equals(requestToken)) {
                    dcatapDataClient.refreshKnownLicences();
                    LOG.info("Successfully refreshed known licences");

                    return Response.status(202).build();
                } else {
                    return Response.status(401).build();
                }
            } catch (JSONException e) {
                LOG.error("Triggering refresh of known licences failed.", e);
                return Response.status(400).build();
            }
        } else {
            LOG.error("Refresh of known licences requested, but no admin token has been configured. Returning status 500.");
            return Response.status(500).build();
        }
    }
}

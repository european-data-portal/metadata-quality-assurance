package de.fhg.fokus.edp.mqa.service.validation;

import com.fasterxml.jackson.databind.JsonNode;
import de.fhg.fokus.edp.mqa.service.model.Violation;

import java.util.Set;

/**
 * Created by bdi on 02/06/15.
 */
public interface ViolationParser {

    /**
     * Validate set.
     *
     * @param jsonNode the json node
     * @return the set
     */
    Set<Violation> validate(JsonNode jsonNode);
}

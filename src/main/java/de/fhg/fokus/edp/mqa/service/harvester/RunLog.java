package de.fhg.fokus.edp.mqa.service.harvester;

import java.time.LocalDateTime;

/**
 * Created by bdi on 29/07/16.
 */
public class RunLog {

    private long id;
    private String message;
    private Severity severity;
    private LocalDateTime timestamp;
    private boolean attachment;
    private ServiceStage stage;
    private Category category;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public ServiceStage getStage() {
        return stage;
    }

    public void setStage(ServiceStage stage) {
        this.stage = stage;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isAttachment() {
        return attachment;
    }

    public void setAttachment(boolean attachment) {
        this.attachment = attachment;
    }


    public enum Severity {
        info("Info"), error("Error"), warning("Warning");

        private final String label;

        Severity(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public enum Category {
        connection("Connection"), transformation("Transformation"), dataset("Dataset"), system("System"), unknown("Unknown");

        private final String label;

        Category(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public enum ServiceStage {
        init_stage("Initialize"),
        export_stage("Export"),
        transformation_stage("Transform"),
        import_stage("Import"),
        filter_stage("Filter"),
        mapping_stage("Mapping"),
        delete_stage("Delete");

        private final String label;

        ServiceStage(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }
}

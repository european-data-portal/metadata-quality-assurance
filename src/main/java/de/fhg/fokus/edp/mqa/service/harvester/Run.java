package de.fhg.fokus.edp.mqa.service.harvester;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by bdi on 28/07/16.
 */
public class Run {
    private long id;
    private String status;
    private LocalDateTime startTime;
    private LocalDateTime endtTime;
    private Duration duration;
    private int numberAdded;
    private int numberUpdated;
    private int numberDeleted;
    private int numberSkipped;
    private int numberRejected;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndtTime() {
        return endtTime;
    }

    public void setEndtTime(LocalDateTime endtTime) {
        this.endtTime = endtTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public int getNumberAdded() {
        return numberAdded;
    }

    public void setNumberAdded(int numberAdded) {
        this.numberAdded = numberAdded;
    }

    public int getNumberUpdated() {
        return numberUpdated;
    }

    public void setNumberUpdated(int numberUpdated) {
        this.numberUpdated = numberUpdated;
    }

    public int getNumberDeleted() {
        return numberDeleted;
    }

    public void setNumberDeleted(int numberDeleted) {
        this.numberDeleted = numberDeleted;
    }

    public int getNumberSkipped() {
        return numberSkipped;
    }

    public void setNumberSkipped(int numberSkipped) {
        this.numberSkipped = numberSkipped;
    }

    public int getNumberRejected() {
        return numberRejected;
    }

    public void setNumberRejected(int numberRejected) {
        this.numberRejected = numberRejected;
    }
}

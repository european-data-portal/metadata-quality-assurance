package de.fhg.fokus.edp.mqa.service.persistence.entity;

import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Distribution;
import de.fhg.fokus.edp.mqa.service.model.Violation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bdi on 13/04/15.
 */
@Entity
@Table(name = "dataset")
@NamedQueries({
        @NamedQuery(name = "Dataset.findAll", query = "SELECT d FROM DatasetEntity d"),
        @NamedQuery(name = "Dataset.getAllCount", query = "SELECT COUNT(d) FROM DatasetEntity d"),
        @NamedQuery(name = "Dataset.getAllCountForCatalog", query = "SELECT COUNT(d) FROM CatalogEntity c JOIN c.datasets d " +
                "WHERE c.instanceId = :instanceId"),
        @NamedQuery(name = "Dataset.findByName", query = "SELECT d FROM DatasetEntity d WHERE d.name = :name"),
        @NamedQuery(name = "Dataset.getNames", query = "SELECT d.name FROM DatasetEntity d"),
        @NamedQuery(name = "Dataset.conformCount", query = "SELECT COUNT(d) FROM DatasetEntity d WHERE d.violations IS EMPTY"),
        @NamedQuery(name = "Dataset.conformCountForCatalog", query = "SELECT COUNT(d) FROM CatalogEntity c JOIN c.datasets d " +
                "WHERE d.violations IS EMPTY AND c.instanceId = :instanceId"),
        @NamedQuery(name = "Dataset.notConformCount", query = "SELECT COUNT(d) FROM DatasetEntity d " +
                "WHERE d.violations IS NOT EMPTY"),
        @NamedQuery(name = "Dataset.listAllNames", query = "SELECT d.name FROM DatasetEntity d"),
        @NamedQuery(name = "Dataset.listAllBeforeDate", query = "SELECT d FROM DatasetEntity d WHERE d.dbUpdate < :date"),
        @NamedQuery(name = "Dataset.listAllBeforeDateCount", query = "SELECT COUNT(d) FROM DatasetEntity d WHERE d.dbUpdate < :date"),
        @NamedQuery(name = "Dataset.licenceMostUsed", query = "SELECT d.licenceId, COUNT(d) FROM DatasetEntity d GROUP BY d.licenceId ORDER BY COUNT(d) DESC"),
        @NamedQuery(name = "Dataset.knownLicenceCount", query = "SELECT COUNT (d) FROM DatasetEntity d " +
                "WHERE EXISTS (SELECT l FROM LicenceEntity l WHERE d.licenceId = l.licenceId)")

})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Dataset.Distributions.Violations",
                attributeNodes = {
                        @NamedAttributeNode("distributions"),
                        @NamedAttributeNode("violations"),
                }),
        @NamedEntityGraph(name = "Dataset.Violations",
                attributeNodes = {
                        @NamedAttributeNode("violations")
                }),
        @NamedEntityGraph(name = "Dataset.Distributions",
                attributeNodes = {
                        @NamedAttributeNode("distributions")
                })
})
public class DatasetEntity implements Dataset {

    private static Logger LOG = LoggerFactory.getLogger(DatasetEntity.class);

    @Transient
    private static final int MAX_COLUMN_LENGTH_NAME_TITLE = 1000;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, length = MAX_COLUMN_LENGTH_NAME_TITLE)
    private String name;

    @Column(name = "title", nullable = false, length = MAX_COLUMN_LENGTH_NAME_TITLE)
    private String title;

    @OneToMany(targetEntity = ViolationEntity.class, fetch = LAZY, cascade = ALL, orphanRemoval = true)
    @JoinColumn(name = "violation_id")
    private Set<Violation> violations;

    @OneToMany(targetEntity = DistributionEntity.class, fetch = LAZY, cascade = ALL, orphanRemoval = true)
    @JoinColumn(name = "distribution_id")
    private Set<Distribution> distributions;

    @Column(name = "db_update", nullable = false)
    private LocalDateTime dbUpdate;

    @Column(name = "instance_id", nullable = false, unique = true)
    private String instanceId;

    @Column(name = "machine_readable", nullable = false)
    private Boolean machineReadable;

    @Column(name = "licence_id")
    private String licenceId;

    /**
     * Instantiates a new Dataset entity.
     */
    public DatasetEntity() {
        this.distributions = new LinkedHashSet<>();
        this.violations = new LinkedHashSet<>();
    }

    /**
     * Pre actions.
     */
    @PrePersist
    @PreUpdate
    public void preActions() {
        this.name = StringUtils.abbreviate(name, MAX_COLUMN_LENGTH_NAME_TITLE);
        this.title = StringUtils.abbreviate(title, MAX_COLUMN_LENGTH_NAME_TITLE);
        this.machineReadable = this.distributions.stream().anyMatch(Distribution::isMachineReadable);
    }

    @PreRemove
    public void tearDown(){
        LOG.debug("Dataset [{}] is going to be removed...", this.name);
    }

    @Override
    public String getInstanceId() {
        return instanceId;
    }

    @Override
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    @Override
    public LocalDateTime getDbUpdate() {
        return dbUpdate;
    }

    @Override
    public void setDbUpdate(LocalDateTime dbUpdate) {
        this.dbUpdate = dbUpdate;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Set<Violation> getViolations() {
        return violations;
    }

    @Override
    public void setViolations(Set<Violation> violations) {
        this.violations = violations;
    }

    @Override
    public Set<Distribution> getDistributions() {
        return distributions;
    }

    @Override
    public void setDistributions(Set<Distribution> distributions) {
        this.distributions = distributions;
    }

    @Override
    public boolean isMachineReadable() {
        return machineReadable;
    }

    @Override
    public void setMachineReadable(boolean machineReadable) {
        this.machineReadable = machineReadable;
    }

    @Override
    public String getLicenceId() {
        return licenceId;
    }

    @Override
    public void setLicenceId(String licenceId) {
        this.licenceId = licenceId;
    }
}

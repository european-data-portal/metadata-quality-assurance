package de.fhg.fokus.edp.mqa.service.dcatap;

import de.fhg.fokus.edp.mqa.service.ckan.DcatapDataClientCkanImpl;

import javax.ejb.Stateless;
import javax.enterprise.inject.New;
import javax.enterprise.inject.Produces;
import java.io.Serializable;

/**
 * Created by bdi on 24/04/15.
 */
@Stateless
public class DcatapDataClientFactory implements Serializable {

    /**
     * Gets dcatap data client.
     *
     * @param dcatapDataClient the dcatap data client
     * @return the dcatap data client
     */
    @Produces
    @DcatapData(DcatapDataClientType.CKAN)
    public DcatapDataClient getDcatapDataClient(@New DcatapDataClientCkanImpl dcatapDataClient) {
        return dcatapDataClient;
    }
}

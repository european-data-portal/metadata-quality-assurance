package de.fhg.fokus.edp.mqa.service.timer;

import de.fhg.fokus.edp.mqa.service.fetch.MetadataFetchService;
import de.fhg.fokus.edp.mqa.service.log.Log;
import org.quartz.*;
import org.slf4j.Logger;

import javax.inject.Inject;

/**
 * Created by bdi on 08/10/15.
 */
@DisallowConcurrentExecution
public class ValidationJob implements InterruptableJob {

    private Thread thread;

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private MetadataFetchService metadataFetchService;


    @Inject
    private MqaScheduler mqaScheduler;

    @Override
    public void interrupt() {
        thread.interrupt();
    }

    @Override
    public void execute(JobExecutionContext context) {
        thread = Thread.currentThread();

        // Fetch, store and validate metadata
        try {
            LOG.info("Starting fetching phase...");
            metadataFetchService.fetch();
            LOG.debug("Last run: [{}]", mqaScheduler.getPreviousOrNextValidationRunDate());
        } catch (Exception e) {
            LOG.error("Error during fetch phase", e);
        }
    }
}

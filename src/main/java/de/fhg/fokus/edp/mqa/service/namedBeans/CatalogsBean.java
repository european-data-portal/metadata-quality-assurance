package de.fhg.fokus.edp.mqa.service.namedBeans;

import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Ordering;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Created by bdi on 27/05/15.
 */
@Named
@SessionScoped
public class CatalogsBean implements Serializable {

    private List<Catalog> catalogs;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;


    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        this.catalogs = vdc.listAllCatalogs();
    }

    /**
     * Gets catalogs.
     *
     * @return the catalogs
     */
    public List<Catalog> getCatalogs() {
        return this.catalogs;
    }

    public List<Catalog> getCatalogsSortedByTitle() {
        Ordering<Catalog> catalogOrdering = Ordering.natural().onResultOf(catalog -> catalog.getName().trim().toLowerCase());
        return ImmutableSortedSet.orderedBy(catalogOrdering).addAll(catalogs).build().asList();
    }

    /**
     * Sets catalogs.
     *
     * @param catalogs the catalogs
     */
    public void setCatalogs(List<Catalog> catalogs) {
        this.catalogs = catalogs;
    }
}

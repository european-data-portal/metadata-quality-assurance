package de.fhg.fokus.edp.mqa.service.config;

/**
 * Created by bdi on 13/04/15.
 */
public interface MqaServiceConfigKeys {

    String CKAN_CHECK_URL = "ckan.check.url";
    String CKAN_BROWSE_URL = "ckan.browse.url";
    String CKAN_BASICAUTH_USERNAME = "ckan.basicauth.username";
    String CKAN_BASICAUTH_PASSWORD = "ckan.basicauth.password";
    String DCATAP_DATACLIENT_PAGE_LIMIT = "dcatap_dataclient.page_limit";
    String TIMER_VALIDATION_INTERVAL = "timer.validation.interval";
    String TIMER_SIMILARITY_INTERVAL = "timer.similarity.interval";
    String URL_CHECKER_CONNECTION_TIMEOUT = "url.checker.connectiontimeout";
    String URL_CHECKER_SOCKET_TIMEOUT = "url.checker.sockettimeout";
    String TRIPLESTORE_URL = "triplestore.url";
    String TRIPLESTORE_GRAPH = "triplestore.graph";
    String TRIPLESTORE_REFERENCE_URI = "triplestore.reference.uri";
    String DASHBOARD_URL_LOCALHOST = "dashboard.url.localhost";
    String REPORT_DIRECTORY = "report.directory";
    String REPORT_LANGUAGES = "report.languages";
    String VALIDATION_SCHEMA_JSON_URL = "validation.schema.json.url";
    String HARVESTER_URL_REST = "harvester.url.rest";
    String MAINTENANCE = "maintenance";
    String HIGHCHARTS_RENDER_URL = "highcharts.url";
    String ADMIN_TOKEN = "admin.token";
    String VALIDATION_LOG_FREQUENCY_IN_MINUTES = "log.interval";
    String FP_DIRECTORY = "fingerprint.dir";
    String FP_SCHEDULE_PATH = "fingerprint.schedule";
    String FP_REMOTE_URL = "fingerprint.url";
    String MACHINE_READABLE_FORMATS_URL = "machinereadableformats.url";
    String URL_CHECK_ENDPOINT = "url.check.endpoint";
    String HOST_URL = "host.url";
    String METRIC_CACHE_URL = "metric.cache.url";
}

package de.fhg.fokus.edp.mqa.service.persistence.entity;

import de.fhg.fokus.edp.mqa.service.model.Violation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bdi on 19/04/15.
 */
@Entity
@Table(name = "violation")
@NamedQueries({
        @NamedQuery(name = "Violation.getAll", query = "SELECT v FROM ViolationEntity v"),
        @NamedQuery(name = "Violation.getAllCount", query = "SELECT COUNT(v) FROM ViolationEntity v"),
        @NamedQuery(name = "Violation.getAllCountForCatalog", query = "SELECT COUNT(v) FROM " +
                "CatalogEntity c JOIN c.datasets d JOIN d.violations v WHERE c.instanceId = :instanceId"),
        @NamedQuery(name = "Violation.getNames", query = "SELECT v.violationName FROM ViolationEntity v"),
        @NamedQuery(name = "Violation.getMostNames", query = "SELECT DISTINCT v.violationName, COUNT(v.violationName) FROM " +
                "ViolationEntity v GROUP BY v.violationName ORDER BY COUNT(v.violationName) DESC"),
        @NamedQuery(name = "Violation.getMostNamesForCatalog", query = "SELECT DISTINCT v.violationName, COUNT(v.violationName) FROM " +
                "CatalogEntity c JOIN c.datasets d JOIN d.violations v WHERE c.instanceId = :instanceId GROUP BY v.violationName ORDER BY COUNT(v.violationName) DESC")
})
public class ViolationEntity implements Violation {

    private static Logger LOG = LoggerFactory.getLogger(ViolationEntity.class);

    private static final int MAX_COLUMN_LENGTH_NAME = 1000;
    private static final int MAX_COLUMN_LENGTH_INSTANCE = 1000;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @Column(name = "violation_name", nullable = false, length = MAX_COLUMN_LENGTH_NAME)
    private String violationName;

    @Column(name = "violation_instance", nullable = false, length = MAX_COLUMN_LENGTH_INSTANCE)
    private String violationInstance;

    @Column(name = "db_update", nullable = false)
    private LocalDateTime dbUpdate;

    /**
     * Update action.
     */
    @PrePersist
    @PreUpdate
    public void updateAction() {
        this.violationName = StringUtils.abbreviate(violationName, MAX_COLUMN_LENGTH_NAME);
        this.violationInstance = StringUtils.abbreviate(violationInstance, MAX_COLUMN_LENGTH_INSTANCE);
        this.dbUpdate = LocalDateTime.now();
    }

    @PreRemove
    public void tearDown(){
        LOG.debug("Violation [{}] is going to be removed...", this.id);
    }

    @Override
    public LocalDateTime getDbUpdate() {
        return dbUpdate;
    }

    @Override
    public void setDbUpdate(LocalDateTime dbUpdate) {
        this.dbUpdate = dbUpdate;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getViolationName() {
        return violationName;
    }

    @Override
    public void setViolationName(String violationName) {
        this.violationName = violationName;
    }

    @Override
    public String getViolationInstance() {
        return violationInstance;
    }

    @Override
    public void setViolationInstance(String violationInstance) {
        this.violationInstance = violationInstance;
    }
}

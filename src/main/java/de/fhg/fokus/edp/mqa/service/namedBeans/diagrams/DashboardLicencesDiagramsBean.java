package de.fhg.fokus.edp.mqa.service.namedBeans.diagrams;

import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.namedBeans.ReportElementBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by fritz on 30.09.16.
 */
@Named
@RequestScoped
public class DashboardLicencesDiagramsBean extends DiagramBean implements Serializable {

    private Set<ReportElementBean> knownLicencesPercentagePieChart;
    private Set<ReportElementBean> mostUsedLicencesColumnChart;
    private Set<ReportElementBean> knownLicencesByCatalogBarChart;

    @Override
    public boolean renderSection() {
        return getRenderMetric("/metric/global/render/licences");
    }

    @Override
    void createCharts() {
        createMostUsedLicenceColumnChart();
        createKnownLicencesPieChart();
        createKnownLicencesByCatalogBarChart();
    }

    private void createMostUsedLicenceColumnChart() {
        mostUsedLicencesColumnChart = getSetWithMultipleValuesSorted( "/metric/global/datasets/licences", 10);
    }

    private void createKnownLicencesPieChart() {
        knownLicencesPercentagePieChart = getSetWithBinaryValues("/metric/global/datasets/known_licences");
    }

    private void createKnownLicencesByCatalogBarChart() {
        knownLicencesByCatalogBarChart = getSetWithMultipleValuesSorted("/metric/global/catalogues/known_licences", Constants.BAR_CHART_ELEMENT_COUNT);
    }

    public Set<ReportElementBean> getKnownLicencesPercentagePieChart() {
        return knownLicencesPercentagePieChart;
    }

    public void setKnownLicencesPercentagePieChart(Set<ReportElementBean> knownLicencesPercentagePieChart) {
        this.knownLicencesPercentagePieChart = knownLicencesPercentagePieChart;
    }

    public Set<ReportElementBean> getMostUsedLicencesColumnChart() {
        return mostUsedLicencesColumnChart;
    }

    public void setMostUsedLicencesColumnChart(Set<ReportElementBean> mostUsedLicencesColumnChart) {
        this.mostUsedLicencesColumnChart = mostUsedLicencesColumnChart;
    }

    public Set<ReportElementBean> getKnownLicencesByCatalogBarChart() {
        return knownLicencesByCatalogBarChart;
    }

    public void setKnownLicencesByCatalogBarChart(Set<ReportElementBean> knownLicencesByCatalogBarChart) {
        this.knownLicencesByCatalogBarChart = knownLicencesByCatalogBarChart;
    }
}

package de.fhg.fokus.edp.mqa.service.persistence;

import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.*;
import de.fhg.fokus.edp.mqa.service.persistence.entity.*;
import de.fhg.fokus.edp.mqa.service.timer.MqaScheduler;
import de.fhg.fokus.edp.mqa.service.utils.Formats;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;

import static de.fhg.fokus.edp.mqa.service.config.Constants.HINT_FETCH_GRAPH;

/**
 * Created by bdi on 13/04/15.
 */
@TransactionScoped
public class ValidationDataClientPersistenceImpl implements ValidationDataClient, Serializable {

    @Inject
    @Log
    private Logger LOG;

    @PersistenceContext
    private EntityManager em;

    @Inject
    private Formats formats;

    /**
     * Instantiates a new Validation data client persistence.
     */
    @Inject
    public ValidationDataClientPersistenceImpl() {
    }

    @Override
    public List listAllDatasets(int firstRow, int rowCount) {
        return em.createNamedQuery("Dataset.findAll").setFirstResult(firstRow)
                .setMaxResults(rowCount).getResultList();
    }

    @Override
    public List listAllDistributions(int firstRow, int rowCount) {
        return em.createNamedQuery("Distribution.findAll").setFirstResult(firstRow)
                .setMaxResults(rowCount).getResultList();
    }

    @Override
    public Dataset getDatasetByName(String name) {
        LOG.trace("Get Dataset [{}]", name);

        try {
            return (Dataset) em.createNamedQuery("Dataset.findByName")
                    .setParameter("name", name)
                    .setHint(HINT_FETCH_GRAPH, em.getEntityGraph("Dataset.Distributions.Violations"))
                    .getSingleResult();
        } catch (NoResultException e) {
            LOG.debug("Query for dataset with name [{}] yielded no results. Returning null.", name);
        } catch (NonUniqueResultException e) {
            LOG.warn("Query for dataset with name [{}] yielded multiple results. Returning null.", name);
        }

        return null;

        /*
        List<Dataset> dbResults = em.createNamedQuery("Dataset.findByName")
                .setParameter("name", name)
                .setHint(HINT_FETCH_GRAPH, em.getEntityGraph("Dataset.Distributions.Violations"))
                .getResultList();

        if (dbResults.isEmpty()) {
            LOG.debug("Query for dataset with name [{}] yielded no results. Returning null.", name);
            return null;
        } else if (dbResults.size() > 1) {
            LOG.warn("Query for dataset with name [{}] yielded [{}] results. Returning first entry.", name, dbResults.size());
        }

        return dbResults.get(0);
        */
    }

    @Override
    public Dataset createDataset() {
        return new DatasetEntity();
    }

    @Override
    public DatasetSimilarity createDatasetSimilarity() {
        return new DatasetSimilarityEntity();
    }

    @Override
    public Catalog createCatalog() {
        return new CatalogEntity();
    }

    @Override
    public Distribution createDistribution() {
        return new DistributionEntity();
    }

    @Override
    public Violation createViolation() {
        return new ViolationEntity();
    }

    @Override
    public Licence createLicence() {
        return new LicenceEntity();
    }

    @Override
    public Catalog getCatalogByName(String name) {
        LOG.trace("Get Catalog [{}]", name);

        try {
            return (Catalog) em.createNamedQuery("Catalog.findByName")
                    .setParameter("name", name)
                    .setHint(HINT_FETCH_GRAPH, em.getEntityGraph("Catalog.Datasets"))
                    .getSingleResult();
        } catch (NoResultException e) {
            LOG.debug("Query for catalog with name [{}] yielded no results. Returning null.", name);
        } catch (NonUniqueResultException e) {
            LOG.warn("Query for catalog with name [{}] yielded multiple results. Returning null.", name);
        }

        return null;
        /*
        List<Catalog> dbResults =  em.createNamedQuery("Catalog.findByName")
                .setParameter("name", name)
                .getResultList();

        if (dbResults.isEmpty()) {
            LOG.warn("Query for catalog with name [{}] yielded no results. Returning null.", name);
            return null;
        } else if (dbResults.size() > 1) {
            LOG.warn("TMP : " + dbResults.size() + " : " + dbResults.get(0).getName());
            LOG.warn("Query for catalog with name [{}] yielded [{}] results. Returning first entry.", name, dbResults.size());
        }

        return dbResults.get(0);
        */
    }

    @Override
    public Catalog getCatalogWithoutDatasetsByName(String name) {
        LOG.trace("Get Catalog [{}]", name);

        //return (Catalog) em.createNamedQuery("Catalog.findByName").setParameter("name", name).getSingleResult();

        List<Catalog> dbResults = em.createNamedQuery("Catalog.findByName").setParameter("name", name).getResultList();

        if (dbResults.isEmpty()) {
            LOG.debug("Query for catalog without datasets with name [{}] yielded no results. Returning null.", name);
            return null;
        } else if (dbResults.size() > 1) {
            LOG.warn("Query for catalog without datasets with name [{}] yielded [{}] results. Returning first entry.", name, dbResults.size());
        }

        return dbResults.get(0);
    }

    @Override
    public List<String> listAllCatalogNames() {
        return (List<String>) em.createNamedQuery("Catalog.getNames").getResultList();
    }

    @Override
    public Distribution getDistributionByInstanceId(String distributionId) {

        /*
        return (Distribution) em.createNamedQuery("Distribution.findByInstanceId")
                .setParameter("instanceId", distributionId)
                .getSingleResult();
        */

        List<Distribution> dbResults = em.createNamedQuery("Distribution.findByInstanceId")
                .setParameter("instanceId", distributionId)
                .getResultList();

        if (dbResults.isEmpty()) {
            LOG.debug("Query for distribution with ID [{}] yielded no results. Returning null.", distributionId);
            return null;
        } else if (dbResults.size() > 1) {
            LOG.warn("Query for distribution with ID [{}] yielded [{}] results. Returning first entry.", distributionId, dbResults.size());
        }

        return dbResults.get(0);
    }


    @Override
    public List listDatasetNamesByCatalogName(String catalogName) {
        return em.createQuery("SELECT d.name FROM CatalogEntity c JOIN c.datasets d WHERE c.name = :name")
                .setParameter("name", catalogName)
                .getResultList();
    }

    @Override
    public List listDistributionsByDatasetName(String name) {
        return em.createQuery("SELECT dist FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist WHERE d.name = :name")
                .setParameter("name", name)
                .getResultList();
    }

    @Override
    public List listViolationsByCatalogName(String name) {
        return em.createQuery("SELECT v FROM CatalogEntity c JOIN c.datasets d JOIN d.violations v WHERE c.name = :name")
                .setParameter("name", name).getResultList();
    }

    @Override
    public List listViolationsByDatasetName(String name) {
        return em.createQuery("SELECT v FROM CatalogEntity c JOIN c.datasets d JOIN d.violations v WHERE d.name = :name").setParameter
                ("name", name).getResultList();
    }

    @Override
    public List listAllViolations() {
        return em.createNamedQuery("Violation.getAll").getResultList();
    }

    @Override
    public int countAllDatasets() {
        int count;

        try {
            count = ((Number) em.createNamedQuery("Dataset.getAllCount").getSingleResult()).intValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public long countViolationsByCatalog(Catalog catalog) {
        int count;

        try {
            count = ((Number) em.createNamedQuery("Violation.getAllCountForCatalog")
                    .setParameter("instanceId", catalog.getInstanceId())
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public int countDatasetsConform(boolean conform) {

        int count;

        try {
            count = conform
                    ? ((Number) em.createNamedQuery("Dataset.conformCount").getSingleResult()).intValue()
                    : ((Number) em.createNamedQuery("Dataset.notConformCount").getSingleResult()).intValue();
        } catch (NoResultException e) {
            LOG.warn("No results found for conform = [{}], return count 0", conform, e);
            count = 0;
        }

        return count;
    }

    @Override
    public Set<Integer> listDistributionStatusCodes() {

        Set<Integer> codes = new LinkedHashSet<>();
        codes.addAll(em.createNamedQuery("Distribution.listStatusAccessCodes").getResultList());
        codes.addAll(em.createNamedQuery("Distribution.listStatusDownloadCodes").getResultList());

        return codes;
    }

    @Override
    @Transactional
    public int countDistributionStatusCodesByStatusCode(int statusCode) {

        int access;
        try {
            access = ((Number) em.createNamedQuery("Distribution.statusCodesAccessCountByStatusCode")
                    .setParameter("status", statusCode)
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            access = 0;
        }

        int download;
        try {
            download = ((Number) em.createNamedQuery("Distribution.statusCodesDownloadCountByStatusCode")
                    .setParameter("status", statusCode)
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            download = 0;
        }

        return download + access;
    }

    @Override
    public int countDistributionsWithDownloadUrl() {
        int count;
        try {
            count = ((Number) em.createNamedQuery("Distribution.withDownloadUrlCount").getSingleResult()).intValue();
        } catch (NoResultException e) {
            count = 0;
        }
        return count;
    }

    @Override
    public int countDistributionsWithUrlOkByUrlType(boolean accessUrl) {
        int count;

        try {
            count = accessUrl
                    ? ((Number) em.createQuery("SELECT COUNT(dist) FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist " +
                    "WHERE dist.statusAccessUrl < 400").getSingleResult()).intValue()
                    : ((Number) em.createQuery("SELECT COUNT(dist) FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist " +
                    "WHERE dist.downloadUrl IS NOT NULL AND dist.statusDownloadUrl < 400").getSingleResult()).intValue();
        } catch (NoResultException e) {
            LOG.warn("No results found for accessUrl = [{}], return count 0", accessUrl, e);
            count = 0;
        }

        return count;
    }

    @Override
    public int countAllDistributions() {
        int count;

        try {
            count = ((Number) em.createNamedQuery("Distribution.getAllCount").getSingleResult()).intValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public int countDistributionsWithStatusCodesOk() {
        int count;

        try {
            count = ((Number) em.createNamedQuery("Distribution.conformAndOk").getSingleResult()).intValue();
        } catch (NoResultException e) {
            count = 0;
        }
        return count;
    }

    @Override
    public Set<String> listViolationNames() {
        Set<String> names = new LinkedHashSet<>();
        names.addAll(em.createNamedQuery("Violation.getNames").getResultList());
        return names;
    }

    @Override
    public Map<String, Long> listViolationMostCommonWithCount(int limit) {
        Map<String, Long> map = new HashMap<>();

        em.createNamedQuery("Violation.getMostNames")
                .setMaxResults(limit)
                .getResultList()
                .stream().forEachOrdered(occurrence -> {
            String key = ((String) ((Object[]) occurrence)[0]).replaceAll("\\d", "*");
            long value = (Long) ((Object[]) occurrence)[1];

            if (map.containsKey(key)) {
                map.put(key, map.get(key) + value);
            } else {
                map.put(key, value);
            }
        });

        return map;
    }

    @Override
    public Map<String, Long> listViolationsMostCommonWithCountByCatalog(Catalog catalog, int limit) {
        Map<String, Long> map = new HashMap<>();

        em.createNamedQuery("Violation.getMostNamesForCatalog")
                .setParameter("instanceId", catalog.getInstanceId())
                .setMaxResults(limit)
                .getResultList()
                .stream().forEachOrdered(occurrence -> {
            String key = ((String) ((Object[]) occurrence)[0]).replaceAll("\\d", "*");
            long value = (Long) ((Object[]) occurrence)[1];

            if (map.containsKey(key)) {
                map.put(key, map.get(key) + value);
            } else {
                map.put(key, value);
            }
        });

        return map;
    }

    @Override
    public Map<Catalog, Long> listAllCatalogsWithNonConformantDatasetCount() {
        Map<Catalog, Long> catalogsWithNonConformantDatasetCount = new HashMap<>();

        em.createNamedQuery("Catalog.noOfNonCompliantDatasetsPerCatalog")
                .getResultList()
                .forEach(catalog ->
                        catalogsWithNonConformantDatasetCount.put((Catalog) ((Object[]) catalog)[0], (Long) ((Object[]) catalog)[1])
                );

        return addMissingCatalogsWithZero(catalogsWithNonConformantDatasetCount);
    }

    @Override
    public Map<Catalog, Long> listCatalogsWithAccessibleDistributions() {
        Map<Catalog, Long> map = new HashMap<>();

        em.createNamedQuery("Catalog.listCatalogsWithAccessibleDatasetsDistCount")
                .getResultList()
                .forEach(catalog ->
                        map.put((Catalog) ((Object[]) catalog)[0], (Long) ((Object[]) catalog)[1]));

        return addMissingCatalogsWithZero(map);
    }

    @Override
    public List<Dataset> listDatasetsWithStatusCodesNotOkByCatalogName(int firstRow, int rowCount, String catalogName) {

        Instant start = Instant.now();

        List datasets = em.createQuery("SELECT DISTINCT d FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist WHERE ((dist.statusAccessUrl < 200 OR dist.statusAccessUrl >= 400) OR (dist.statusDownloadUrl < 200 OR dist.statusDownloadUrl >= 400)) AND c.name = :name")
                .setParameter("name", catalogName)
                .setFirstResult(firstRow)
                .setMaxResults(rowCount)
                .getResultList();

        LOG.debug("Selection of datasets with non accessible distribution from catalog [{}] took: {}", catalogName, Duration.between(start, Instant.now()));

        return datasets;
    }

    @Override
    public int countDatasetsWithStatusCodesNotOkByCatalogName(String catalogName) {
        int count;

        try {
            count = ((Number) em.createQuery("SELECT COUNT(DISTINCT d) FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist WHERE (dist.statusAccessUrl >= 400 OR dist.statusDownloadUrl >= 400) AND c.name = :name")
                    .setParameter("name", catalogName)
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public long countDatasetsByCatalog(Catalog catalog) {
        int count;

        try {
            count = ((Number) em.createNamedQuery("Dataset.getAllCountForCatalog")
                    .setParameter("instanceId", catalog.getInstanceId())
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public long countDatasetsConformByCatalog(Catalog catalog) {
        int count;

        try {
            count = ((Number) em.createNamedQuery("Dataset.conformCountForCatalog")
                    .setParameter("instanceId", catalog.getInstanceId())
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public List<Dataset> listDatasetsNonConformByCatalogName(int firstRow, int rowCount, String catalogName) {

        Instant start = Instant.now();

        List datasets = em.createQuery("SELECT DISTINCT d FROM CatalogEntity c JOIN c.datasets d JOIN d.violations v WHERE d.violations IS NOT EMPTY AND c.name = :name")
                .setParameter("name", catalogName)
                .setFirstResult(firstRow)
                .setMaxResults(rowCount)
                .getResultList();

        LOG.debug("Selection of datasets with non accessible distribution from catalog [{}] took: {}", catalogName, Duration.between(start, Instant.now()));

        return datasets;
    }

    @Override
    public int countDatasetsNonConformByCatalogName(String catalogName) {
        int count;

        try {
            count = ((Number) em.createQuery("SELECT COUNT(d) FROM CatalogEntity c JOIN c.datasets d WHERE d.violations IS NOT EMPTY AND c" +
                    ".name = :name").setParameter("name", catalogName)
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            count = 0;
        }
        return count;
    }

    @Override
    public int countDistributionsByCatalogName(String catalogName) {
        int count;

        try {
            count = ((Number) em.createQuery("SELECT COUNT(dist) FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist WHERE c.name = :name")
                    .setParameter("name", catalogName)
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public List listAllCatalogs() {
        return em.createNamedQuery("Catalog.findAll").getResultList();
    }

    @Override
    public long countAllCatalogs() {
        int count;

        try {
            count = ((Number) em.createNamedQuery("Catalog.countAll")
                    .getSingleResult())
                    .intValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public List listAllCatalogsWithPaging(int firstRow, int rowCount) {
        return em.createNamedQuery("Catalog.findAll")
                .setFirstResult(firstRow).setMaxResults(rowCount).getResultList();
    }

    @Override
    public List listAllCatalogNamesWithPaging(int firstRow, int rowCount) {
        return em.createNamedQuery("Catalog.listAllNames")
                .setFirstResult(firstRow).setMaxResults(rowCount).getResultList();
    }

    @Override
    public List listAllDatasetNamesWithPaging(int firstRow, int rowCount) {
        return em.createNamedQuery("Dataset.listAllNames")
                .setFirstResult(firstRow).setMaxResults(rowCount).getResultList();
    }

    @Override
    public List listAllDistributionIdsWithPaging(int firstRow, int rowCount) {
        return em.createNamedQuery("Distribution.listAllInstanceIds")
                .setFirstResult(firstRow).setMaxResults(rowCount).getResultList();
    }

    @Override
    public long countAllViolations() {
        long count;

        try {
            count = ((Number) em.createNamedQuery("Violation.getAllCount").getSingleResult()).longValue();
        } catch (NoResultException e) {
            count = 0;
        }

        return count;
    }

    @Override
    public boolean isDistributionExistingById(String instanceId) {
        return getDistributionByInstanceId(instanceId) != null;
    }

    @Override
    public boolean isDatasetSimilarityExisting(DatasetSimilarity datasetSimilarity) {
        List similarities = em.createNamedQuery("DatasetSimilarity.isExisting")
                .setParameter("datasetInstanceIdA", datasetSimilarity.getDatasetInstanceIdA())
                .setParameter("datasetInstanceIdB", datasetSimilarity.getDatasetInstanceIdB())
                .getResultList();

        return !similarities.isEmpty();
    }

    @Override
    public long countDistributionsMachineReadable() {
        return ((Number) em.createNamedQuery("Distribution.machineReadableCount").getSingleResult()).longValue();
    }

    @Override
    public Map<String, Long> listDistributionFormatsMostUsed(int max) {
        Map<String, Long> map = new HashMap<>();

        em.createNamedQuery("Distribution.listUsedFormats")
                .setMaxResults(max)
                .getResultList()
                .forEach(format ->
                    map.put((String) ((Object[]) format)[0], (Long) ((Object[]) format)[1]));

        return replaceEmptyStringKeys(map);
    }

    @Override
    public Map<String, Long> listDistributionFormatsMostUsedByCatalog(Catalog catalog, int max) {
        Map<String, Long> map = new HashMap<>();

        em.createNamedQuery("Distribution.listUsedFormatsForCatalog")
                .setParameter("instanceId", catalog.getInstanceId())
                .setMaxResults(max)
                .getResultList()
                .stream().forEachOrdered(format ->
                map.put((String) ((Object[]) format)[0], (Long) ((Object[]) format)[1]));

        return replaceEmptyStringKeys(map);
    }

    @Override
    public Map<Catalog, Long> listCatalogsWithMachineReadableDistributionsCount() {
        Map<Catalog, Long> catalogsWithMachineReadableDistributionsCount = new HashMap<>();

        em.createQuery("SELECT c, COUNT(dist) FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist WHERE dist.machineReadable = TRUE GROUP BY c")
                .getResultList()
                .forEach(catalog ->
                        catalogsWithMachineReadableDistributionsCount.put((Catalog) ((Object[]) catalog)[0], (Long) ((Object[]) catalog)[1]));

        return addMissingCatalogsWithZero(catalogsWithMachineReadableDistributionsCount);
    }

    @Override
    public Map<Catalog, Long> listCatalogsWithMachineReadableDatasetsCount() {
        Map<Catalog, Long> catalogsWithMachineReadableDatasetsCount = new HashMap<>();

        em.createQuery("SELECT c, COUNT(d) FROM CatalogEntity c JOIN c.datasets d WHERE d.machineReadable = true GROUP BY c")
                .getResultList()
                .forEach(catalog ->
                        catalogsWithMachineReadableDatasetsCount.put((Catalog) ((Object[]) catalog)[0], (Long) ((Object[]) catalog)[1]));

        return addMissingCatalogsWithZero(catalogsWithMachineReadableDatasetsCount);
    }

    @Override
    public Map<String, Long> listCatalogIdsWithDistributionCount() {
        Map<String, Long> map = new HashMap<>();

        em.createQuery("SELECT c.instanceId, COUNT(dist) FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist GROUP BY c.instanceId ORDER BY COUNT(dist) DESC")
                .getResultList()
                .stream().forEachOrdered(catalogInstanceId ->
                map.put((String) ((Object[]) catalogInstanceId)[0], (Long) ((Object[]) catalogInstanceId)[1]));

        return map;
    }

    @Override
    public Map<String, Long> listCatalogIdsWithDatasetCount() {
        Map<Catalog, Long> map = new HashMap<>();

        em.createQuery("SELECT c, COUNT(d) FROM CatalogEntity c JOIN c.datasets d GROUP BY c")
                .getResultList()
                .forEach(catalog ->
                map.put((Catalog) ((Object[]) catalog)[0], (Long) ((Object[]) catalog)[1]));

        Map<String, Long> result = new HashMap<>();
        addMissingCatalogsWithZero(map).forEach((catalog, count) -> result.put(catalog.getInstanceId(), count));
        return result;
    }

    @Override
    public long countDistributionsAvailableByCatalogId(String catalogInstanceId) {
        return ((Number) em.createQuery("SELECT COUNT(dist) FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist " +
                "WHERE dist.statusAccessUrl < 400 AND (dist.downloadUrl IS NULL OR dist.statusDownloadUrl < 400) AND c.instanceId = :instanceId")
                .setParameter("instanceId", catalogInstanceId)
                .getSingleResult())
                .longValue();
    }

    @Override
    public Map<Integer, Long> listDistributionStatusCodesWithCountByCatalog(Catalog catalog) {
        Map<Integer, Long> map = new HashMap<>();

        em.createNamedQuery("Distribution.listStatusAccessCodesByCatalog")
                .setParameter("instanceId", catalog.getInstanceId())
                .getResultList()
                .stream().forEachOrdered(errorCode ->
                map.put((Integer) ((Object[]) errorCode)[0], (Long) ((Object[]) errorCode)[1]));

        em.createNamedQuery("Distribution.listStatusDownloadCodesByCatalog")
                .setParameter("instanceId", catalog.getInstanceId())
                .getResultList()
                .stream().forEachOrdered(errorCode -> {
            int statusCode = (Integer) ((Object[]) errorCode)[0];
            long count = (Long) ((Object[]) errorCode)[1];

            if (map.containsKey(statusCode)) {
                map.put(statusCode, map.get(statusCode) + count);
            } else {
                map.put(statusCode, count);
            }
        });

        return map;
    }

    @Override
    public long countDistributionsWithDownloadUrlByCatalog(Catalog catalog) {
        return ((Number) em.createNamedQuery("Distribution.countWithDownloadUrlByCatalog")
                .setParameter("instanceId", catalog.getInstanceId())
                .getSingleResult())
                .longValue();
    }

    @Override
    public long countMachineReadableDistributionsByCatalog(Catalog catalog) {
        return ((Number) em.createNamedQuery("Distribution.getMachineReadableDistributionsCountOfCatalog")
                .setParameter("instanceId", catalog.getInstanceId())
                .getSingleResult())
                .longValue();
    }

    @Override
    public long countMachineReadableDatasetsByCatalog(Catalog catalog) {
        return ((Number) em.createQuery("SELECT COUNT(d) FROM CatalogEntity c JOIN c.datasets d WHERE :instanceId = c.instanceId AND d.machineReadable = true")
                .setParameter("instanceId", catalog.getInstanceId())
                .getSingleResult())
                .longValue();
    }

    @Override
    public long countMachineReadableDatasets() {
        return ((Number) em.createQuery("SELECT COUNT(d) FROM DatasetEntity d WHERE d.machineReadable = true")
                .getSingleResult())
                .longValue();
    }

    @Override
    public Licence getLicenceById(String licenceId) {
        List<Licence> dbResults = em.createNamedQuery("Licence.getLicenceById")
                .setParameter("licenceId", licenceId)
                .getResultList();

        if (dbResults.size() == 0) {
            LOG.debug("Query for licence with ID [{}] yielded no results. Returning null.", licenceId);
            return null;
        } else if (dbResults.size() > 1) {
            LOG.warn("Query for licence with ID [{}] yielded [{}] results. Returning first entry.", licenceId, dbResults.size());
        }

        return dbResults.get(0);
    }

    @Override
    public int countAllLicences() {
        return ((Number) em.createNamedQuery("Licence.getAllCount").getSingleResult()).intValue();
    }

    @Override
    public long countLicencesNonNullByCatalogName(String catalogName) {
        return ((Number) em.createNamedQuery("Catalog.licenceCount")
                .setParameter("name", catalogName)
                .getSingleResult())
                .longValue();
    }

    @Override
    public Map<String, Long> listMostUsedLicencesWithCount(int max) {
        Map<String, Long> result = new HashMap<>();

        em.createNamedQuery("Dataset.licenceMostUsed")
                .setMaxResults(max)
                .getResultList()
                .forEach(licence ->
                        result.put((String) ((Object[]) licence)[0], (Long) ((Object[]) licence)[1]));

        return replaceEmptyStringKeys(result);
    }

    @Override
    public Map<String, Long> listMostUsedLicencesWithCountByCatalogName(int max, String catalogName) {
        Map<String, Long> result = new LinkedHashMap<>();

        em.createNamedQuery("Catalog.mostUsedLicencesByCatalogName")
                .setParameter("name", catalogName)
                .setMaxResults(max)
                .getResultList()
                .forEach(licence ->
                        result.put((String) ((Object[]) licence)[0], (Long) ((Object[]) licence)[1]));

        return replaceEmptyStringKeys(result);
    }

    @Override
    public Map<Catalog, Long> listCatalogsWithMostDatasetsOfKnownLicenceWithCount() {
        Map<Catalog, Long> catalogsWithKnownLicences = new LinkedHashMap<>();

        em.createNamedQuery("Catalog.datasetsWithKnownLicencesCount")
                .getResultList()
                .forEach(catalog ->
                        catalogsWithKnownLicences.put((Catalog) ((Object[]) catalog)[0], (Long) ((Object[]) catalog)[1]));

        // Sort map by value and limit it to given limit value
        return addMissingCatalogsWithZero(catalogsWithKnownLicences);
    }

    @Override
    public long countDatasetsWithKnownLicences() {
        return (long) em.createNamedQuery("Dataset.knownLicenceCount").getSingleResult();
    }

    @Override
    public long countDatasetsWithKnownLicencesByCatalogName(String catalogName) {
        return (long) em.createNamedQuery("Catalog.knownLicencesCountByCatalogName")
                .setParameter("name", catalogName)
                .getSingleResult();
    }

    @Override
    @Transactional
    public void removeOutdatedEntities(LocalDateTime date) {
        List<Catalog> catalogs = em.createNamedQuery("Catalog.modifiedBeforeDate").setParameter("date", date).getResultList();
        catalogs.forEach(this::removeEntity);
        LOG.info("Removed {} outdated catalogs that where last updated before {}", catalogs.size(), date);

        int totalDsCount = 0;
        final int deletionPageSize = 50;

        // use float to prevent integer division not suitable for rounding
        int numberOfPages = (int) Math.ceil(countAllDatasets() / (float) deletionPageSize);

        for (int i = 0; i < numberOfPages; i++) {
            for (Object dataset : em.createNamedQuery("Dataset.listAllBeforeDate")
                    .setParameter("date", date)
                    .setMaxResults(deletionPageSize)
                    .setFirstResult(i)
                    .getResultList()) {
                LOG.trace("Deleted DS [{}] from [{}]", ((DatasetEntity) dataset).getName(), ((DatasetEntity) dataset).getDbUpdate());
                removeEntity(dataset);
                totalDsCount++;
            }
        }

        LOG.info("Removed {} outdated datasets that where last updated before {}", totalDsCount, date);
    }

    @Override
    @Transactional
    public <T> T persistEntity(T entity) {
        if (entity != null) {
            LOG.trace("Persisting entity [{}]", entity);
            if (entity instanceof DistributionEntity) {
                Distribution dist = (Distribution) entity;
                dist.setMachineReadable(formats.isFormatMachineReadable(dist.getFormat()));
                entity = (T) dist;
            }
            entity = dbUpdateReflection(entity);
            em.persist(entity);
        } else {
            LOG.error("Could not persist null entity");
        }
        return entity;
    }

    @Override
    @Transactional
    public <T> T updateEntity(T entity) {
        if (entity instanceof DistributionEntity) {
            Distribution dist = (Distribution) entity;
            dist.setMachineReadable(formats.isFormatMachineReadable(dist.getFormat()));
            entity = (T) dist;
        }
        entity = dbUpdateReflection(entity);
        return em.merge(entity);
    }

    private <T> T dbUpdateReflection(T entity) {
        try {
            BeanUtils.setProperty(entity, "dbUpdate", LocalDateTime.now());
        } catch (IllegalAccessException | InvocationTargetException e) {
            LOG.error("Error during update entity reflection", e);
        }
        return entity;
    }

    @Override
    @Transactional
    public <T> void removeEntity(T entity) {
        if (entity != null) {
            LOG.trace("Removing entity [{}]", entity);
            em.remove(em.merge(entity));
        } else {
            LOG.warn("Could not remove null entity");
        }
    }

    private Map<String, Long> replaceEmptyStringKeys(Map<String, Long> map) {
        Map<String, Long> mapWithReplacedKey = new LinkedHashMap<>();

        for (Map.Entry entry : map.entrySet()) {
            String key = (String) entry.getKey();
            long value = (Long) entry.getValue();
            if (key != null && !key.isEmpty()) {
                mapWithReplacedKey.put(key, value);
            } else {
                mapWithReplacedKey.put("?", value);
            }
        }

        return mapWithReplacedKey;
    }

    /**
     * Adds all catalogs not present in a given map and assigns a value of zero
     * @param existingCatalogs
     * @return a map with all available catalogs as keys with their respective values
     */
    private Map<Catalog, Long> addMissingCatalogsWithZero(Map<Catalog, Long> existingCatalogs) {
        List<String> allCatalogNames = listAllCatalogNames();
        existingCatalogs.forEach((catalog, count) -> allCatalogNames.remove(catalog.getName()));
        allCatalogNames.forEach(name -> existingCatalogs.put(getCatalogByName(name), 0L));
        return existingCatalogs;
    }
}
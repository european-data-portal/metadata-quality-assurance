package de.fhg.fokus.edp.mqa.service.namedBeans;

import de.fhg.fokus.edp.mqa.service.fetch.MqaHttpProvider;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType.PERSISTENCE;

/**
 * Created by bdi on 27/05/15.
 */
@RequestScoped
@Named
public class CurrentCatalogBean implements Serializable {

    @Inject
    private MqaHttpProvider httpProvider;

    private String catalogId;
    private String catalogName;
    private String catalogTitle;

    private int datasetsNotAccessible = 0;
    private int datasetsNonConformant = 0;

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> paramMap = context.getExternalContext().getRequestParameterMap();

        catalogName = paramMap.get("catalogName");

        if (catalogName != null) {
            httpProvider.getMetric("/info/catalogues").ifPresent(json -> {
                if (json.getBoolean("success")) {
                    for (JsonValue c : json.getJsonArray("result")) {
                        JsonObject catalogue = (JsonObject) c;

                        if (catalogName.equals(catalogue.getString("name"))) {
                            catalogId = catalogue.getString("id");
                            catalogTitle = catalogue.getString("title");
                            break;
                        }
                    }
                }
            });

            datasetsNotAccessible = getCount("/metric/catalogues/" + catalogId + "/datasets/notaccessible_count");
            datasetsNonConformant = getCount("/metric/catalogues/" + catalogId + "/datasets/nonconformant_count");
        }
    }

    public int getAffectedDatasetsWithDistributionIssues() {
        return datasetsNotAccessible;
    }

    public void setAffectedDatasetsWithDistributionIssues(int datasetsNotAccessible) {
        this.datasetsNotAccessible = datasetsNotAccessible;
    }

    public int getAffectedDatasetsWithViolationIssues() {
        return datasetsNonConformant;
    }

    public void setAffectedDatasetsWithViolationIssues(int datasetsNonConformant) {
        this.datasetsNonConformant = datasetsNonConformant;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public String getCatalogTitle() {
        return catalogTitle;
    }

    public void setCatalogTitle(String catalogTitle) {
        this.catalogTitle = catalogTitle;
    }

    private Integer getCount(String endpoint) {
        AtomicInteger distributionCount = new AtomicInteger(0);

        httpProvider.getMetric(endpoint).ifPresent(json -> {
            if (json.getBoolean("success")) {
                distributionCount.set(json.getInt("result"));
            }
        });

        return distributionCount.get();
    }
}

package de.fhg.fokus.edp.mqa.service.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.Base64;

/**
 * Created by bdi on 24/12/15.
 */
@ApplicationScoped
public class Authenticator implements ClientRequestFilter {

    private String username;
    private String password;

    public void filter(ClientRequestContext clientRequestContext) throws IOException {
        MultivaluedMap<String, Object> headers = clientRequestContext.getHeaders();
        String headerAuth = String.format("Basic %s", Base64.getEncoder().encodeToString(String.format("%s:%s", username,
                password)
                .getBytes()));
        headers.add("content-type", "application/json");
        headers.add("authorization", headerAuth);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

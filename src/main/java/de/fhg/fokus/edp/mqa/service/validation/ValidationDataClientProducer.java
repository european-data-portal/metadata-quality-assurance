package de.fhg.fokus.edp.mqa.service.validation;

import de.fhg.fokus.edp.mqa.service.persistence.ValidationDataClientPersistenceImpl;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.New;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by bdi on 10/04/15.
 */
@ApplicationScoped
public class ValidationDataClientProducer implements Serializable {

    /**
     * Instantiates a new Validation data client producer.
     */
    @Inject
    public ValidationDataClientProducer() {
    }

    /**
     * Gets validation data client.
     *
     * @param dataClientPersistence the data client persistence
     * @return the validation data client
     */
    @Produces
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    public ValidationDataClient getValidationDataClient(@New ValidationDataClientPersistenceImpl dataClientPersistence) {
        return dataClientPersistence;
    }
}

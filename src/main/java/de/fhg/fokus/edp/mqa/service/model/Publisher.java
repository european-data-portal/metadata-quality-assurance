package de.fhg.fokus.edp.mqa.service.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by bdi on 16/04/15.
 */
public interface Publisher extends Serializable {

    /**
     * Gets id.
     *
     * @return the id
     */
    long getId();

    /**
     * Gets name.
     *
     * @return the name
     */
    String getName();

    /**
     * Sets name.
     *
     * @param name the name
     */
    void setName(String name);

    /**
     * Gets email.
     *
     * @return the email
     */
    String getEmail();

    /**
     * Sets email.
     *
     * @param email the email
     */
    void setEmail(String email);

    /**
     * Gets db update.
     *
     * @return the db update
     */
    LocalDateTime getDbUpdate();

    /**
     * Sets db update.
     *
     * @param dbUpdate the db update
     */
    void setDbUpdate(LocalDateTime dbUpdate);
}

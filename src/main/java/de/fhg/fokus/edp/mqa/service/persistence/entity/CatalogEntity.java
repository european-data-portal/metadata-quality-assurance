package de.fhg.fokus.edp.mqa.service.persistence.entity;

import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Publisher;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bdi on 17/04/15.
 */
@Entity
@Table(name = "catalog")
@NamedQueries({
        @NamedQuery(name = "Catalog.findAll", query = "SELECT c FROM CatalogEntity c"),
        @NamedQuery(name = "Catalog.countAll", query = "SELECT COUNT(c) FROM CatalogEntity c"),
        @NamedQuery(name = "Catalog.findByName", query = "SELECT c FROM CatalogEntity c WHERE c.name = :name"),
        @NamedQuery(name = "Catalog.getNames", query = "SELECT c.name FROM CatalogEntity c"),
        @NamedQuery(name = "Catalog.listAllNames", query = "SELECT c.name FROM CatalogEntity c"),
        @NamedQuery(name = "Catalog.noOfNonCompliantDatasetsPerCatalog", query = "SELECT c, COUNT(d) FROM " +
                "CatalogEntity c JOIN c.datasets d WHERE d.violations IS NOT EMPTY GROUP BY c"),
        @NamedQuery(name = "Catalog.datasetCount", query = "SELECT COUNT(c.datasets) FROM CatalogEntity c"),
        @NamedQuery(name = "Catalog.listCatalogsWithAccessibleDatasetsDistCount", query = "SELECT c, COUNT(dist) FROM CatalogEntity c " +
                "JOIN c.datasets d JOIN d.distributions dist WHERE (dist.statusDownloadUrl < 400 OR dist.downloadUrl IS NULL) AND " +
                "dist.statusAccessUrl < 400 GROUP BY c ORDER BY COUNT(dist) DESC"),
        @NamedQuery(name = "Catalog.licenceCount", query = "SELECT COUNT(d) FROM CatalogEntity c JOIN c.datasets d " +
                "WHERE c.name = :name AND d.licenceId IS NOT NULL"),
        @NamedQuery(name = "Catalog.datasetsWithKnownLicencesCount", query = "SELECT c, COUNT(d) FROM CatalogEntity c JOIN c.datasets d " +
                "WHERE EXISTS (SELECT l FROM LicenceEntity l WHERE d.licenceId = l.licenceId) GROUP BY c ORDER BY COUNT(d) DESC"),
        @NamedQuery(name = "Catalog.knownLicencesCountByCatalogName", query = "SELECT COUNT(d) FROM CatalogEntity c JOIN c.datasets d " +
                "WHERE c.name = :name AND EXISTS (SELECT l FROM LicenceEntity l WHERE d.licenceId = l.licenceId)"),
        @NamedQuery(name = "Catalog.mostUsedLicencesByCatalogName",
                query = "SELECT d.licenceId, COUNT(d) FROM CatalogEntity c JOIN c.datasets d WHERE c.name = :name GROUP BY d.licenceId ORDER BY COUNT(d) DESC"),
        @NamedQuery(name = "Catalog.modifiedBeforeDate",
                query = "SELECT c FROM CatalogEntity c WHERE c.dbUpdate < :date"),
        @NamedQuery(name = "Catalog.modifiedBeforeDateCount",
                query = "SELECT COUNT(c) FROM CatalogEntity c WHERE c.dbUpdate < :date")
})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Catalog.Datasets",
                attributeNodes = {
                        @NamedAttributeNode(value = "datasets")
                })
})
public class CatalogEntity implements Catalog, Comparable {

    private static Logger LOG = LoggerFactory.getLogger(CatalogEntity.class);

    @Transient
    private static final int MAX_COLUMN_LENGTH_NAME = 255;

    @Transient
    private static final int MAX_COLUMN_LENGTH_TITLE = 300;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @Column(name = "name", nullable = false, unique = true, length = MAX_COLUMN_LENGTH_NAME)
    private String name;

    @Column(name = "title", nullable = false, length = MAX_COLUMN_LENGTH_TITLE)
    private String title;

    @OneToMany(targetEntity = DatasetEntity.class, fetch = LAZY, cascade = ALL)
    @JoinColumn(name = "dataset_id")
    private List<Dataset> datasets;

    // TODO: mapped by catalog...
    @OneToOne(targetEntity = PublisherEntity.class)
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;

    @Column(name = "spatial", length = MAX_COLUMN_LENGTH_TITLE)
    private String spatial;

    @Column(name = "description")
    private String description;

    @Column(name = "db_update", nullable = false)
    private LocalDateTime dbUpdate;

    @Column(name = "instance_id", nullable = false, unique = true)
    private String instanceId;

    /**
     * Instantiates a new Catalog entity.
     */
    public CatalogEntity() {
        this.datasets = new ArrayList<>();
    }

    @PreRemove
    public void tearDown(){
        LOG.debug("Catalog [{}] is going to be removed...", this.name);
    }

    @Override
    public String getInstanceId() {
        return instanceId;
    }

    @Override
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    /**
     * Update action.
     */
    @PrePersist
    @PreUpdate
    public void updateAction() {
        this.name = StringUtils.abbreviate(name, MAX_COLUMN_LENGTH_NAME);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public LocalDateTime getDbUpdate() {
        return dbUpdate;
    }

    @Override
    public void setDbUpdate(LocalDateTime dbUpdate) {
        this.dbUpdate = dbUpdate;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<Dataset> getDatasets() {
        return datasets;
    }

    @Override
    public void setDatasets(List<Dataset> datasets) {
        this.datasets = datasets;
    }

    @Override
    public Publisher getPublisher() {
        return publisher;
    }

    @Override
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getSpatial() {
        return spatial;
    }

    public void setSpatial(String langCode) {
        this.spatial = langCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(Object o) {
        return this.title.compareToIgnoreCase(((Catalog) o).getTitle());
    }


}

package de.fhg.fokus.edp.mqa.service.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by bdi on 02/04/15.
 */
@ApplicationPath("/api")
public class MqaServiceApplication extends Application {
    public Set<Class<?>> getClasses() {
        return new HashSet<>(Arrays.asList(DataReadActions.class, AdminActions.class, UrlCheckCallback.class));
    }
}

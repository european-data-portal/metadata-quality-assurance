package de.fhg.fokus.edp.mqa.service.namedBeans.diagrams;

import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.namedBeans.ReportElementBean;
import org.slf4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by bdi on 28/05/15.
 */
@Named
@RequestScoped
public class DashboardDistributionsDiagramsBean extends DiagramBean implements Serializable {

    @Inject
    @Log
    private Logger LOG;

    private Set<ReportElementBean> distributionsUrlPieChart;
    private Set<ReportElementBean> distributionsStatusPieChart;
    private Set<ReportElementBean> distributionsCatalogAvailabilityBarChart;
    private Set<ReportElementBean> distributionsWithoutDownloadUrl;
    private Set<ReportElementBean> distributionsMachineReadablePercentagePieChart;
    private Set<ReportElementBean> mostUsedDistributionFormatsColumnChart;
    private Set<ReportElementBean> machineReadableDistributionCountPerCatalogBarChart;

    @Override
    void createCharts() {
        createDistributionsUrlPieChart();
        createDistributionsStatusPieChart();
        createDistributionsAvailabilityByCatalog();
        createDistributionsWithDownloadUrl();
        createDistributionsMachineReadablePercentagePieChart();
        createMostUsedDistributionFormatsColumnChart();
        createMachineReadableDistributionCountPerCatalogBarChart();
    }

    @Override
    public boolean renderSection() {
        return getRenderMetric("/metric/global/render/distributions");
    }

    private void createMachineReadableDistributionCountPerCatalogBarChart() {
        machineReadableDistributionCountPerCatalogBarChart = getSetWithMultipleValuesSorted( "/metric/global/catalogues/machine_readable", Constants.BAR_CHART_ELEMENT_COUNT);
    }

    private void createMostUsedDistributionFormatsColumnChart() {
        mostUsedDistributionFormatsColumnChart = getSetWithMultipleValuesSorted( "/metric/global/distributions/formats", 10);
    }

    private void createDistributionsMachineReadablePercentagePieChart() {
        distributionsMachineReadablePercentagePieChart = getSetWithBinaryValues("/metric/global/datasets/machine_readable");
    }

    private void createDistributionsWithDownloadUrl() {
        distributionsWithoutDownloadUrl = getSetWithBinaryValues("/metric/global/distributions/download_url_exists");

    }

    private void createDistributionsAvailabilityByCatalog() {
        distributionsCatalogAvailabilityBarChart = getSetWithMultipleValuesSorted("/metric/global/catalogues/accessibility", Constants.BAR_CHART_ELEMENT_COUNT);
    }

    private void createDistributionsUrlPieChart() {
        distributionsUrlPieChart = getSetWithDynamicKeysAndMultipleValues("/metric/global/distributions/accessibility");
    }

    private void createDistributionsStatusPieChart() {
        distributionsStatusPieChart = getSetWithFixedKeysAndMultipleValues("/metric/global/distributions/status_codes");
    }

    /**
     * Gets distributions url pie chart.
     *
     * @return the distributions url pie chart
     */
    public Set<ReportElementBean> getDistributionsUrlPieChart() {
        return distributionsUrlPieChart;
    }

    /**
     * Sets distributions url pie chart.
     *
     * @param distributionsUrlPieChart the distributions url pie chart
     */
    public void setDistributionsUrlPieChart(Set<ReportElementBean> distributionsUrlPieChart) {
        this.distributionsUrlPieChart = distributionsUrlPieChart;
    }

    /**
     * Gets distributions status pie chart.
     *
     * @return the distributions status pie chart
     */
    public Set<ReportElementBean> getDistributionsStatusPieChart() {
        return distributionsStatusPieChart;
    }

    /**
     * Sets distributions status pie chart.
     *
     * @param distributionsStatusPieChart the distributions status pie chart
     */
    public void setDistributionsStatusPieChart(Set<ReportElementBean> distributionsStatusPieChart) {
        this.distributionsStatusPieChart = distributionsStatusPieChart;
    }

    /**
     * Gets distributions catalogId availability bar chart.
     *
     * @return the distributions catalogId availability bar chart
     */
    public Set<ReportElementBean> getDistributionsCatalogAvailabilityBarChart() {
        return distributionsCatalogAvailabilityBarChart;
    }

    /**
     * Sets distributions catalogId availability bar chart.
     *
     * @param distributionsCatalogAvailabilityBarChart the distributions catalogId availability bar chart
     */
    public void setDistributionsCatalogAvailabilityBarChart(Set<ReportElementBean> distributionsCatalogAvailabilityBarChart) {
        this.distributionsCatalogAvailabilityBarChart = distributionsCatalogAvailabilityBarChart;
    }

    /**
     * Gets distributions without download url.
     *
     * @return the distributions without download url
     */
    public Set<ReportElementBean> getDistributionsWithoutDownloadUrl() {
        return distributionsWithoutDownloadUrl;
    }

    /**
     * Sets distributions without download url.
     *
     * @param distributionsWithoutDownloadUrl the distributions without download url
     */
    public void setDistributionsWithoutDownloadUrl(Set<ReportElementBean> distributionsWithoutDownloadUrl) {
        this.distributionsWithoutDownloadUrl = distributionsWithoutDownloadUrl;
    }

    /**
     * Gets distributions machine readable percentage pie chart.
     *
     * @return the distributions machine readable percentage pie chart
     */
    public Set<ReportElementBean> getDistributionsMachineReadablePercentagePieChart() {
        return distributionsMachineReadablePercentagePieChart;

    }

    /**
     * Sets distributions machine readable percentage pie chart.
     *
     * @param distributionsMachineReadablePercentagePieChart the distributions machine readable percentage pie chart
     */
    public void setDistributionsMachineReadablePercentagePieChart(Set<ReportElementBean> distributionsMachineReadablePercentagePieChart) {
        this.distributionsMachineReadablePercentagePieChart = distributionsMachineReadablePercentagePieChart;

    }

    /**
     * Gets most used distribution formats column chart.
     *
     * @return the most used distribution formats column chart
     */
    public Set<ReportElementBean> getMostUsedDistributionFormatsColumnChart() {
        return mostUsedDistributionFormatsColumnChart;
    }

    /**
     * Gets machine readable distribution count per catalogId bar chart.
     *
     * @return the machine readable distribution count per catalogId bar chart
     */
    public Set<ReportElementBean> getMachineReadableDistributionCountPerCatalogBarChart() {
        return machineReadableDistributionCountPerCatalogBarChart;
    }
}

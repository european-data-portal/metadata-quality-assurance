package de.fhg.fokus.edp.mqa.service.timer;

import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.similarities.SimilarityOutputfileParser;
import de.fhg.fokus.paneodp.similarities.DataCollector;
import org.quartz.*;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by bdi on 08/10/15.
 */
@DisallowConcurrentExecution
public class DatasetSimilarityJob implements InterruptableJob {

    private Thread thread;

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.TRIPLESTORE_URL)
    private String triplestoreEndpoint;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.TRIPLESTORE_GRAPH)
    private String triplestoreGraph;

    @Inject
    private SimilarityOutputfileParser similarityOutputfileParser;


    @Override
    public void interrupt() throws UnableToInterruptJobException {
        thread.interrupt();
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        thread = Thread.currentThread();

        try {
            String similaritiesFile = Files.createTempFile("similarities", ".csv")
                    .toAbsolutePath().toString();
            DataCollector dc = new DataCollector(triplestoreEndpoint, triplestoreGraph, similaritiesFile);
            dc.findSimilarities();
            similarityOutputfileParser.parseAndPersist(similaritiesFile);
        } catch (IOException | ClassNotFoundException e) {
            LOG.error("Error during similarity collection.", e);
        }
    }
}

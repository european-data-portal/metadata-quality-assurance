package de.fhg.fokus.edp.mqa.service.dcatap;

import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Licence;

import javax.json.JsonArray;
import java.util.List;

/**
 * Created by bdi on 24/04/15.
 */
public interface DcatapDataClient {

    /**
     * Gets datasets by catalog.
     *
     * @param catalogName the catalog name
     * @param limit       the limit
     * @param page        the page
     * @return the datasets by catalog
     */
    List<Dataset> getDatasetsByCatalog(String catalogName, int limit, int page);

    /**
     * Gets catalogs.
     *
     * @return the catalogs
     */
    List getCatalogs();

    void refreshKnownLicences();
}

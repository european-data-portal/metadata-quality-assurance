package de.fhg.fokus.edp.mqa.service.report;

import com.google.common.io.ByteStreams;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.canvas.draw.DottedLine;
import com.itextpdf.kernel.pdf.extgstate.PdfExtGState;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.layout.LayoutContext;
import com.itextpdf.layout.layout.LayoutResult;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.property.TabAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import com.itextpdf.layout.renderer.ParagraphRenderer;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import org.jsoup.Jsoup;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fritz on 16.11.16.
 */
@RequestScoped
@Named
public class PdfReportGenerator extends ReportGenerator {

    @Inject
    private ChartGenerator chartGenerator;

    private PdfDocument pdfReport;
    private Document document;
    private PageSize pageSize;

    private List<AbstractMap.SimpleEntry<String, AbstractMap.SimpleEntry<String, Integer>>> toc;

    private String distributionHeader, violationHeader, licenceHeader;

    private final static String EDP_LOGO = "img/mqa_report_header.png";

    private PdfFont bold;

    @Override
    void generateReport(String language) {

        reloadLanguageSpecificData();

        distributionHeader = rb.getString("dashboard.distribution.heading");
        violationHeader = rb.getString("dashboard.compliance.heading");
        licenceHeader = rb.getString("dashboard.licence.heading");

        try {
            setReportFile(ReportHandler.ReportFormat.PDF, language);

            // pdf config
            bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);

            // variables
            toc = new ArrayList<>();

            // create pdf file
            PdfWriter pdfWriter = new PdfWriter(reportFile.getAbsolutePath(),
                    new WriterProperties().addXmpMetadata().setPdfVersion(PdfVersion.PDF_1_7));

            pdfReport = new PdfDocument(pdfWriter);
            pageSize = new PageSize(PageSize.A4);
            document = new Document(pdfReport);
            document.setMargins(60, 60, 60, 60);

            // set meta data
            PdfDocumentInfo info = pdfReport.getDocumentInfo();
            info.setTitle(dashboardTitle);
            info.setAuthor(rb.getString("common.edp.title"));
            info.setKeywords(rb.getString("report.pdf.keywords"));

            // watermark
            //IEventHandler handler = new TransparentImage();
            //pdfReport.addEventHandler(PdfDocumentEvent.START_PAGE, handler);

            // header and footer
            pdfReport.addEventHandler(PdfDocumentEvent.START_PAGE, new Header(dashboardTitle));
            PageXofY pageNumberEvent = new PageXofY();
            pdfReport.addEventHandler(PdfDocumentEvent.END_PAGE, pageNumberEvent);

            // create title page
            createTitlePage();

            // create info page
            createInfoPage();

            // create dashboard pages
            createDashboardPages();

            // create catalog pages
            for (Catalog catalog : catalogs) {
                reloadCatalogDiagrams(String.valueOf(catalog.getId()));
                createCatalogPages(catalog);
            }

            // generate toc and page numbers
            createPdfToc();
            pageNumberEvent.writeTotal(pdfReport);

            //Close document
            document.close();
            pdfReport.close();

            LOG.info("Created PDF report file: {}", reportFile.getAbsolutePath());
        } catch (IOException e) {
            LOG.error("Failed to generate PDF report for language [{}].", language, e);
        }
    }

    /**
     * Creates the first report page. If the image which is meant to appear in the center of the page
     * cannot be loaded, a text message is rendered instead.
     */
    private void createTitlePage() {
        Paragraph p = new Paragraph();

        Image edpLogo = createImgFromFile(EDP_LOGO);
        if (edpLogo != null) {
            edpLogo.setAutoScale(true);
            p.add(edpLogo);
            p.add(dashboardTitle).setFontSize(16).setTextAlignment(TextAlignment.CENTER);
            document.add(p.setVerticalAlignment(VerticalAlignment.MIDDLE));
        }

        /*
        // add date of last run to bottom right of page
        p = new Paragraph(dashboardLastUpdate + ": " + lastRun)
                .setHorizontalAlignment(HorizontalAlignment.RIGHT)
                .setVerticalAlignment(VerticalAlignment.BOTTOM)
                .setTextAlignment(TextAlignment.RIGHT);
        document.add(p);
        */


        // add current date to title page
        p = new Paragraph(dashboardCurrentDate + ": " + currentDate)
                .setTextAlignment(TextAlignment.CENTER);
        document.add(p);

        document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
    }

    // generates a new page which contains introductory text about the available statistics
    private void createInfoPage() {
        newTocEntry(rb.getString("dashboard.heading"));
        addIntroText(rb.getString("dashboard.heading"), rb.getString("report.pdf.dashboard.introduction"));
    }

    // generates the pages needed to fit all diagrams from the MQAs general dashboard
    private void createDashboardPages() throws IOException {
        newTocEntry(dashboardOverview);
        Paragraph p = new Paragraph(dashboardOverview).setFontSize(16).setFont(bold).setTextAlignment(TextAlignment.CENTER);
        document.add(p);
        newLine();

        ChartBundle bundle, left, right;

        // distributions
        addIntroText(distributionHeader, rb.getString("dashboard.distribution.introduction"));

        if (renderGlobalDistributions) {
            left = new ChartBundle.Builder(dashboardDistributionsDiagramsBean.getDistributionsUrlPieChart(), ChartBundle.ChartType.PIE)
                    .title(distAccessibilityAll).build();
            right = new ChartBundle.Builder(dashboardDistributionsDiagramsBean.getDistributionsStatusPieChart(), ChartBundle.ChartType.PIE)
                    .title(distStatusCodes).build();
            addTwoChartsWithTextInColumns(rb.getString("dashboard.help.overallaccessibility"), rb.getString("dashboard.help.errorstatuscodes"), left, right, 1, 1);

            document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));

            left = new ChartBundle.Builder(dashboardDistributionsDiagramsBean.getDistributionsWithoutDownloadUrl(), ChartBundle.ChartType.PIE)
                    .title(distWithoutDownloadUrl).build();
            right = new ChartBundle.Builder(dashboardDistributionsDiagramsBean.getDistributionsMachineReadablePercentagePieChart(), ChartBundle.ChartType.PIE)
                    .title(distMachineReadablePercentage).build();
            addTwoChartsWithTextInColumns(rb.getString("dashboard.help.distwithdownloadurl"), rb.getString("dashboard.help.distribution.format.machinereadable.percentage"), left, right, 1, 1);

            bundle = new ChartBundle.Builder(dashboardDistributionsDiagramsBean.getDistributionsCatalogAvailabilityBarChart(), ChartBundle.ChartType.BAR)
                    .title(distAccessibilityCatalog).valueName(rb.getString("dashboard.distribution.available")).build();
            addFullWidthChart(bundle, rb.getString("dashboard.help.catalogswithaccessibledistributions"));

            bundle = new ChartBundle.Builder(dashboardDistributionsDiagramsBean.getMostUsedDistributionFormatsColumnChart(), ChartBundle.ChartType.COLUMN)
                    .title(distFormatMostUsed).valueName(rb.getString("distributions.format.name")).yAxisLabel(rb.getString("common.percentage")).build();
            addFullWidthChart(bundle, rb.getString("dashboard.help.distribution.format.mostused"));

            bundle = new ChartBundle.Builder(dashboardDistributionsDiagramsBean.getMachineReadableDistributionCountPerCatalogBarChart(), ChartBundle.ChartType.BAR)
                    .title(distMachineReadableCount).valueName(rb.getString("dashboard.distribution.format.machinereadable.catalog.count.percent")).build();
            addFullWidthChart(bundle, rb.getString("dashboard.help.distribution.format.machinereadable.catalog.count"));
        } else {
            addSimpleTextBlock(rb.getString("dashboard.nodistributions"));
        }

        // violations
        addIntroText(violationHeader, rb.getString("dashboard.compliance.description"));

        if (renderGlobalViolations) {
            left = new ChartBundle.Builder(dashboardViolationsDiagramsBean.getViolationMostOccurredPieChart(), ChartBundle.ChartType.PIE)
                    .title(violationMostOccurred).build();
            right = new ChartBundle.Builder(dashboardViolationsDiagramsBean.getDatasetsConformPieChart(), ChartBundle.ChartType.PIE)
                    .title(violationComplianceAll).build();
            addTwoChartsWithTextInColumns(rb.getString("dashboard.help.violationoccurrences"), rb.getString("dashboard.help.compliantdatasets"), left, right, 1, 1);

            bundle = new ChartBundle.Builder(dashboardViolationsDiagramsBean.getViolationsCountByCatalogBarChart(), ChartBundle.ChartType.BAR)
                    .title(violationComplianceCatalog).valueName(rb.getString("dashboard.compliance.non_compliant_dataset_per_catalog_in_percentage")).build();
            addFullWidthChart(bundle, rb.getString("dashboard.help.catalogwithnoncompliantdatasets"));
        } else {
            addSimpleTextBlock(rb.getString("dashboard.compliance.all_datasets_conform_catalog"));
        }

        // licences
        addIntroText(licenceHeader, rb.getString("dashboard.licence.description"));

        if (renderGlobalLicences) {
            left = new ChartBundle.Builder(dashboardLicencesDiagramsBean.getKnownLicencesPercentagePieChart(), ChartBundle.ChartType.PIE)
                    .title(licenceKnownPercentage).build();
            right = new ChartBundle.Builder(dashboardLicencesDiagramsBean.getMostUsedLicencesColumnChart(), ChartBundle.ChartType.COLUMN)
                    .title(licenceMostUsed).valueName(rb.getString("licence.name.singular")).yAxisLabel(rb.getString("common.percentage")).build();
            addTwoChartsWithTextInColumns(rb.getString("dashboard.help.licence.known.percentage"), rb.getString("dashboard.help.licence.mostused"), left, right, 1, 2);

            document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));

            bundle = new ChartBundle.Builder(dashboardLicencesDiagramsBean.getKnownLicencesByCatalogBarChart(), ChartBundle.ChartType.BAR)
                    .title(licenceCatalogsWithMostKnown).valueName(rb.getString("dashboard.licence.catalogue.known.subtitle")).build();
            addFullWidthChart(bundle, rb.getString("dashboard.help.licence.catalog.known.percentage"));
        } else {
            addSimpleTextBlock(rb.getString("dashboard.licence.charts_not_applicable"));
        }

        document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
    }

    // generates the pages needed to fit all diagrams belonging to a catalog
    private void createCatalogPages(Catalog catalog) throws IOException {
        newTocEntry(catalog.getTitle());

        Paragraph p = new Paragraph(catalog.getTitle())
                .setFontSize(16)
                .setFont(bold)
                .setTextAlignment(TextAlignment.CENTER)
                .setKeepWithNext(true);
        p.setKeepTogether(true); // not needed?;

        document.add(p);

        ChartBundle bundle, left, right;

        // generates the pages needed to fit all diagrams from the MQAs general dashboard
        addDataHeader(distributionHeader);

        if (catalogDashboardDistributionBean.renderSection()) {
            bundle = new ChartBundle.Builder(catalogDashboardDistributionBean.getAvailableDistributionsPercentagePieChart(), ChartBundle.ChartType.PIE)
                    .title(distAccessibilityAll).build();

            if (!catalogDashboardDistributionBean.getErrorCodesPieChart().isEmpty()) {
                right = new ChartBundle.Builder(catalogDashboardDistributionBean.getErrorCodesPieChart(), ChartBundle.ChartType.PIE)
                        .title(distStatusCodes).build();
                addTwoChartsWithTextInColumns(rb.getString("dashboard.help.overallaccessibility"), rb.getString("dashboard.help.errorstatuscodes"), bundle, right, 1, 1);
            } else {
                addFullWidthChart(bundle, rb.getString("dashboard.help.overallaccessibility"));
            }

            document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));

            left = new ChartBundle.Builder(catalogDashboardDistributionBean.getWithDownloadUrlPieChart(), ChartBundle.ChartType.PIE)
                    .title(distWithoutDownloadUrl).build();
            right = new ChartBundle.Builder(catalogDashboardDistributionBean.getMachineReadableDatasetPieChart(), ChartBundle.ChartType.PIE)
                    .title(distMachineReadablePercentage).build();
            addTwoChartsWithTextInColumns(rb.getString("dashboard.help.distwithdownloadurl"), rb.getString("dashboard.help.distribution.format.machinereadable.percentage"), left, right, 1, 1);

            bundle = new ChartBundle.Builder(catalogDashboardDistributionBean.getMostUsedFormats(), ChartBundle.ChartType.COLUMN)
                    .title(distFormatMostUsed)
                    .yAxisLabel(rb.getString("common.percentage"))
                    .valueName(rb.getString("distributions.format.name"))
                    .build();
            addFullWidthChart(bundle, rb.getString("dashboard.help.distribution.format.mostused"));

        } else {
            addSimpleTextBlock(rb.getString("dashboard.nodistributions"));
        }
        document.add(new Paragraph());

        // Violations statistics
        addDataHeader(violationHeader);

        if (catalogDashboardViolationBean.renderSection()) {
            left = new ChartBundle.Builder(catalogDashboardViolationBean.getMostOccurredViolationsPieChart(), ChartBundle.ChartType.PIE)
                    .title(violationMostOccurred).build();
            right = new ChartBundle.Builder(catalogDashboardViolationBean.getConformDatasetPieChart(), ChartBundle.ChartType.PIE)
                    .title(violationComplianceAll).build();
            addTwoChartsWithTextInColumns(rb.getString("dashboard.help.violationoccurrences"), rb.getString("dashboard.help.compliantdatasets"), left, right, 1, 1);
        } else {
            addSimpleTextBlock(rb.getString("dashboard.compliance.all_datasets_conform_catalog"));
        }

        // Licence statistics
        addDataHeader(licenceHeader);

        if (catalogDashboardLicenceBean.renderSection()) {
            left = new ChartBundle.Builder(catalogDashboardLicenceBean.getRatioKnownUnknownPieChart(), ChartBundle.ChartType.PIE)
                    .title(licenceKnownPercentage).build();
            right = new ChartBundle.Builder(catalogDashboardLicenceBean.getMostUsedLicencesColumnChart(), ChartBundle.ChartType.COLUMN)
                    .title(licenceMostUsed).yAxisLabel(rb.getString("common.percentage")).valueName(rb.getString("licence.name.plural")).build();
            addTwoChartsWithTextInColumns(rb.getString("dashboard.help.licence.known.percentage"), rb.getString("dashboard.help.licence.mostused"), left, right, 1, 2);
        } else {
            addSimpleTextBlock(rb.getString("dashboard.licence.charts_not_applicable"));
        }

        document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
    }

    // helper method for adding text to the intro page
    private void addIntroText(String title, String text) {
        addDataHeader(title);
        addSimpleTextBlock(htmlToText(text));
    }

    // helper method for setting headers separating the various data blocks
    private void addDataHeader(String text) {
        Paragraph p = new Paragraph(text).setFontSize(14).setKeepWithNext(true);
        document.add(p);
    }

    // helper method for adding regular blocks of text
    private void addSimpleTextBlock(String text) {
        Paragraph p = new Paragraph(text);
        p.setFontSize(12);
        document.add(p);
        newLine();
    }

    /**
     * Adds a new, borderless table to the document containing four cells (2x2).
     * Ratios can be set to allow for dynamic spacing
     *
     * @param textLeft   the text visible in the upper left cell
     * @param textRight  the text visible in the upper right cell
     * @param left       the image visible in the bottom left cell
     * @param right      the image visible in the bottom right cell
     * @param ratioLeft  the weight the left column will have
     * @param ratioRight the weight the right column will have
     * @throws IOException
     */
    private void addTwoChartsWithTextInColumns(String textLeft, String textRight, ChartBundle left, ChartBundle right, int ratioLeft, int ratioRight) throws IOException {
        // calculate ratio values
        int totalWidth = ratioLeft + ratioRight;
        float leftWidth = (pageSize.getWidth() / totalWidth) * ratioLeft;
        float rightWidth = (pageSize.getWidth() / totalWidth) * ratioRight;

        // initAllLanguages table
        Table table = new Table(new float[]{leftWidth, rightWidth});
        table.setWidthPercent(100).setBorder(Border.NO_BORDER);

        // add content to table
        table.addCell(new Cell().add(htmlToText(textLeft)).setMargin(10).setBorder(Border.NO_BORDER));
        table.addCell(new Cell().add(htmlToText(textRight)).setMargin(10).setBorder(Border.NO_BORDER));

        Image leftImage = generateImage(left);
        Image rightImage = generateImage(right);

        if (leftImage != null) {
            leftImage.setAutoScale(true);
            table.addCell(new Cell().add(leftImage).setBorder(Border.NO_BORDER));
        }

        if (rightImage != null) {
            rightImage.setAutoScale(true);
            table.addCell(new Cell().add(rightImage).setBorder(Border.NO_BORDER));
        }

        document.add(table);
        newLine();
        newLine();
    }

    // adds text and an image spanning the whole width of the document
    private void addFullWidthChart(ChartBundle bundle, String text) throws IOException {
        document.add(new Paragraph(text));

        Image image = generateImage(bundle);

        if (image != null)
            document.add(generateImage(bundle).setAutoScale(true));

        newLine();
    }

    // generates an Image object from a ChartBundle, using the ChartGenerator class
    private Image generateImage(ChartBundle bundle) {
        File imgFile = chartGenerator.generateChartFile(bundle);
        Image img = null;

        if (imgFile != null) {
            try {
                img = new Image(ImageDataFactory.create(imgFile.getAbsolutePath()));

                // clean up
                Files.delete(Paths.get(imgFile.getAbsolutePath()));
            } catch (Exception e) {
                LOG.warn("Conversion of chart file to iText image failed for chart with title [{}].", bundle.getTitle(), e.getMessage());
            }
        }
        return img;
    }

    // adds a new entry to the table of contents, using the entry name specified
    private void newTocEntry(String tocAnchor) {
        Paragraph p = new Paragraph("");
        AbstractMap.SimpleEntry<String, Integer> page = new AbstractMap.SimpleEntry<>(tocAnchor, pdfReport.getNumberOfPages());
        p.setDestination(tocAnchor).setNextRenderer(new UpdatePageRenderer(p, page));
        document.add(p);
        toc.add(new AbstractMap.SimpleEntry<>(tocAnchor, page));
    }

    // generates the table of contents for the document. Must be called after all other pages have been added.
    private void createPdfToc() throws IOException {
        Paragraph p = new Paragraph().setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD))
                .add(rb.getString("report.pdf.toc.header")).setDestination("toc");
        document.add(p);

        toc.remove(0);
        List<TabStop> tabStops = new ArrayList<>();
        tabStops.add(new TabStop(580, TabAlignment.RIGHT, new DottedLine()));
        for (AbstractMap.SimpleEntry<String, AbstractMap.SimpleEntry<String, Integer>> entry : toc) {
            AbstractMap.SimpleEntry<String, Integer> text = entry.getValue();
            p = new Paragraph()
                    .addTabStops(tabStops)
                    .add(text.getKey())
                    .add(new Tab())
                    .add(String.valueOf(text.getValue()))
                    .setAction(PdfAction.createGoTo(entry.getKey()));
            document.add(p);
        }
    }

    // removes html tags from strings
    private String htmlToText(String htmlText) {
        return Jsoup.parse(htmlText).text();
    }

    // adds an empty paragraph which to enable spacing
    private void newLine() {
        document.add(new Paragraph(" "));
    }

    // creates img object from resource file
    private Image createImgFromFile(String path) {
        try {
            // load file from resources directory
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(path);

            // convert file to byte array required for next step
            byte[] img = ByteStreams.toByteArray(inputStream);

            // create image file and add to paragraph
            return new Image(ImageDataFactory.create(img));
        } catch (IOException e) {
            LOG.warn("Could not load Image from [{}]", path);
        }

        return null;
    }

    // required for the table of contents
    private class UpdatePageRenderer extends ParagraphRenderer {
        protected AbstractMap.SimpleEntry<String, Integer> entry;

        public UpdatePageRenderer(Paragraph modelElement, AbstractMap.SimpleEntry<String, Integer> entry) {
            super(modelElement);
            this.entry = entry;
        }

        @Override
        public LayoutResult layout(LayoutContext layoutContext) {
            LayoutResult result = super.layout(layoutContext);
            entry.setValue(layoutContext.getArea().getPageNumber());
            return result;
        }
    }

    protected class Header implements IEventHandler {
        String header;

        public Header(String header) {
            this.header = header;
        }

        @Override
        public void handleEvent(Event event) {
            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            PdfDocument pdf = docEvent.getDocument();
            PdfPage page = docEvent.getPage();

            if (pdf.getPageNumber(page) > 1) {
                Rectangle pageSize = page.getPageSize();
                PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdf);
                Canvas canvas = new Canvas(pdfCanvas, pdf, pageSize);
                canvas.showTextAligned(header,
                        pageSize.getWidth() / 2,
                        pageSize.getTop() - 30, TextAlignment.CENTER);
            }
        }
    }

    // handles displaying of page numbers
    protected class PageXofY implements IEventHandler {

        protected PdfFormXObject placeholder;
        protected float side = 20;
        protected float xUrl = 50;
        protected float xPageNumber = 500;
        protected float y = 25;
        protected float space = 4.5f;
        protected float descent = 3;

        public PageXofY() {
            placeholder = new PdfFormXObject(new Rectangle(0, 0, side, side));
        }

        @Override
        public void handleEvent(Event event) {
            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            PdfDocument pdf = docEvent.getDocument();
            PdfPage page = docEvent.getPage();

            int pageNumber = pdf.getPageNumber(page);
            if (pageNumber > 2) {
                Rectangle pageSize = page.getPageSize();
                PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdf);
                Canvas canvas = new Canvas(pdfCanvas, pdf, pageSize);

                // edp url
                Paragraph p = new Paragraph(rb.getString("common.edp.url"));
                canvas.showTextAligned(p, xUrl, y, TextAlignment.LEFT);

                // page xPageNumber of y
                p = new Paragraph().add(String.valueOf(pageNumber)).add(" /");
                canvas.showTextAligned(p, xPageNumber, y, TextAlignment.RIGHT);
                pdfCanvas.addXObject(placeholder, xPageNumber + space, y - descent);

                pdfCanvas.release();
            }
        }

        public void writeTotal(PdfDocument pdf) {
            Canvas canvas = new Canvas(placeholder, pdf);
            canvas.showTextAligned(String.valueOf(pdf.getNumberOfPages()),
                    0, descent, TextAlignment.LEFT);
        }
    }

    // Add EDP watermark to pages
    protected class TransparentImage implements IEventHandler {

        protected PdfExtGState gState;
        protected Image img;

        public TransparentImage() {
            img = createImgFromFile(EDP_LOGO);
            gState = new PdfExtGState().setFillOpacity(0.2f);
        }

        @Override
        public void handleEvent(Event event) {
            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            PdfDocument pdf = docEvent.getDocument();
            PdfPage page = docEvent.getPage();

            if (pdf.getPageNumber(page) > 1) {
                PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdf);
                pdfCanvas.saveState().setExtGState(gState);
                Canvas canvas = new Canvas(pdfCanvas, pdf, page.getPageSize());
                canvas.add(img.setAutoScale(true));
                pdfCanvas.restoreState();
                pdfCanvas.release();
            }
        }
    }
}

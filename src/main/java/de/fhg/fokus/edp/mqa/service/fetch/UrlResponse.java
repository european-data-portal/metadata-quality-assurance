package de.fhg.fokus.edp.mqa.service.fetch;

import java.time.Duration;

/**
 * Created by fritz on 01.06.17.
 */
public class UrlResponse {

    private int statusCode;
    private String message;
    private String mimeType;
    private Duration duration;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}

package de.fhg.fokus.edp.mqa.service.timer;

import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.duplicates.FingerprintScheduleParser;
import de.fhg.fokus.edp.mqa.service.duplicates.FingerprintService;
import de.fhg.fokus.edp.mqa.service.log.Log;
import org.quartz.*;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.HashSet;

/**
 * Created by fritz on 10.02.17.
 */
@DisallowConcurrentExecution
public class FingerprintJob implements InterruptableJob {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    MqaScheduler mqaScheduler;

    @Inject
    private FingerprintService fingerprintService;

    @Inject
    private FingerprintScheduleParser parser;

    private Thread thread;

    @Override
    public void interrupt() throws UnableToInterruptJobException {
        thread.interrupt();
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        thread = Thread.currentThread();

        // retrieve language codes
        String langCodeString = context.getJobDetail().getJobDataMap().getString(Constants.JOB_LANG_KEY);

        // parse language codes to set
        HashSet<String> languageCodes = parser.parseLanguageCodes(langCodeString);

        // start fingerprinting
        fingerprintService.fingerprintLanguage(languageCodes, context.getTrigger());
    }
}

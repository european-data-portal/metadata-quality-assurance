package de.fhg.fokus.edp.mqa.service.utils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.fhg.fokus.edp.mqa.service.log.Log;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by bdi on 20/05/15.
 */
@ApplicationScoped
public class MapperUtils implements Serializable {

    @Inject
    @Log
    private Logger LOG;

    private ObjectMapper objectMapper;
    private JsonFactory jsonFactory;

    /**
     * Instantiates a new Mapper utils.
     */
    public MapperUtils() {
        objectMapper = new ObjectMapper();
        jsonFactory = objectMapper.getFactory();
    }

    /**
     * Converts a string formatted JSON text into a JsonNode object.
     *
     * @param nodeString The string formatted JSON text
     * @return The JsonNode object if the conversion was successful. Returns an empty JsonNode if the conversion fails.
     */
    public JsonNode convertToJsonNode(String nodeString) {
        JsonNode jsonNode;

        try {
            JsonParser jp = jsonFactory.createParser(nodeString);
            jsonNode = objectMapper.readTree(jp);
        } catch (IOException e) {
            // create empty list
            jsonNode = new ObjectNode(JsonNodeFactory.instance);
            LOG.error("Cannot convert String [{}] to JsonNode", nodeString, e);
        }

        return jsonNode;
    }

    public String prettyPrintJsonString(String unformattedJson) {
        try {
            Object json = objectMapper.readValue(unformattedJson, Object.class);
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
        } catch (IOException e) {
            LOG.debug("Failed to pretty print JSON String. Returning unformatted input.");
            return unformattedJson;
        }
    }

    public String convertJavaObjectToString(Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

    public String extractJsonFromResponse(Response response) {
        String packagesNodes = response.readEntity(String.class);
        response.close();
        return packagesNodes;
    }

    public boolean isSuccess(JsonNode result) {
        JsonNode success = result.get("success");
        return (success != null && success.isBoolean()) && success
                .booleanValue();
    }

    /**
     * Returns the extracted results of the given json response or an empty json array if no results are present
     *
     * @param node JsonNode to extract results
     * @return Results or empty json array
     */
    public JsonNode getSearchResult(JsonNode node) {
        if (isSuccess(node)) {
            JsonNode result = node.get("result");
            try {
                if (result != null) {
                    JsonNode results = result.get("results");
                    if (results != null && results.isArray()) {
                        if (getCount(node) > 0) {
                            return results;
                        }
                    } else {
                        return result;
                    }
                }
            } catch (NullPointerException e) {
                LOG.warn("Error while parsing search results: {}", e);
            }
        }
        return objectMapper.createArrayNode();
    }

    public int getCount(JsonNode node) {
        JsonNode result = node.get("result");
        if (result != null) {
            return result.get("count").intValue();
        }
        return 0;
    }
}

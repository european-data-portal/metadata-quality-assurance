package de.fhg.fokus.edp.mqa.service.namedBeans.diagrams;

import de.fhg.fokus.edp.mqa.service.namedBeans.ReportElementBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by bdi on 25/01/16.
 */
@RequestScoped
@Named
public class CatalogDashboardViolationBean extends CatalogDiagramBean implements Serializable {

    private Set<ReportElementBean> mostOccurredViolationsPieChart;
    private Set<ReportElementBean> conformDatasetPieChart;

    @Override
    public boolean renderSection() {
        return getRenderMetric("/metric/catalogues/" + catalogId + "/render/violations");
    }

    @Override
    void createCharts() {
        mostOccurredViolationsPieChart = getSetWithFixedKeysAndMultipleValues("/metric/catalogues/" + catalogId + "/datasets/violations");
        conformDatasetPieChart = getSetWithBinaryValues("/metric/catalogues/" + catalogId + "/datasets/compliance");

    }

    /**
     * Gets most occurred violations pie chart.
     *
     * @return the most occurred violations pie chart
     */
    public Set<ReportElementBean> getMostOccurredViolationsPieChart() {
        return mostOccurredViolationsPieChart;
    }

    /**
     * Sets most occurred violations pie chart.
     *
     * @param mostOccurredViolationsPieChart the most occurred violations pie chart
     */
    public void setMostOccurredViolationsPieChart(Set<ReportElementBean> mostOccurredViolationsPieChart) {
        this.mostOccurredViolationsPieChart = mostOccurredViolationsPieChart;
    }

    /**
     * Gets conform dataset pie chart.
     *
     * @return the conform dataset pie chart
     */
    public Set<ReportElementBean> getConformDatasetPieChart() {
        return conformDatasetPieChart;
    }

    /**
     * Sets conform dataset pie chart.
     *
     * @param conformDatasetPieChart the conform dataset pie chart
     */
    public void setConformDatasetPieChart(Set<ReportElementBean> conformDatasetPieChart) {
        this.conformDatasetPieChart = conformDatasetPieChart;
    }
}

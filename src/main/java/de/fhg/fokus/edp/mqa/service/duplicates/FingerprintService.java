package de.fhg.fokus.edp.mqa.service.duplicates;

import com.google.common.base.Joiner;
import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.timer.MqaScheduler;
import org.quartz.Trigger;
import org.slf4j.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.New;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;

/**
 * Created by fritz on 13.03.17.
 */
//TODO integrate into FingerprintJob??
@Dependent
public class FingerprintService implements Serializable {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private MqaScheduler mqaScheduler;

    @Inject
    private SimilarityService similarityService;

    @Resource
    private ManagedExecutorService managedExecutorService;

    @Inject
    @New
    private Instance<FingerprintTask> fingerprintTaskInstance;

    private HashSet<FingerprintTask> fingerprintTasks;

    public void fingerprintLanguage(Set<String> langCodes, Trigger jobKey) {
        fingerprintTasks = new HashSet<>();

        langCodes.forEach(code -> {
            // check for running tasks so no two task write to the same file
            if (!mqaScheduler.getRunningLangCodes().contains(code)) {
                FingerprintTask task = fingerprintTaskInstance.get();
                task.setLangCode(code);
                task.setTrigger(jobKey);
                fingerprintTasks.add(task);
                mqaScheduler.getRunningLangCodes().add(code);
                LOG.debug("Added fingerprinting task for language [{}]", code);
            } else {
                LOG.warn("Fingerprinting for language [{}] is already running. Skipping request", code);
            }
        });

        // add gathered tasks to executor
        CompletionService<FingerprintTaskResponse> completionService = new ExecutorCompletionService<>(managedExecutorService);
        fingerprintTasks.forEach(completionService::submit);
        LOG.info("Submitted [{}] languages for fingerprinting", fingerprintTasks.size());

        try {
            int remainingTasks = fingerprintTasks.size();
            ArrayList<String> failedLanguages = new ArrayList<>();

            while (!fingerprintTasks.isEmpty()) {
                // retrieve language code of completed tasks and remove from global list
                FingerprintTaskResponse fingerprintTaskResponse = completionService.take().get();
                String processedLangCode = fingerprintTaskResponse.getLangCode();
                mqaScheduler.getRunningLangCodes().remove(processedLangCode);
                remainingTasks--;
                LOG.debug("Tasks still running: [{}/{}]", remainingTasks, fingerprintTasks.size());

                // fp was successful if file name is not null
                if (fingerprintTaskResponse.getFileName() != null) {
                    // reload similarity service with new files
                    similarityService.initSingleLanguage(fingerprintTaskResponse.getFileName().toFile());
                } else {
                    // if fp was not successful, register language for rescheduling
                    failedLanguages.add(processedLangCode);
                }

                // check if all fingerprinting tasks are done (completionService.take() will block while waiting)
                if (remainingTasks <= 0)
                    break;
            }

            LOG.info("Out of [{}] submitted tasks [{}] were unsuccessful", langCodes.size(), failedLanguages.size());
            // rescheduling failed languages requires rebuilding comma separated String to prevent clutter of overloaded methods
            if (!failedLanguages.isEmpty())
                mqaScheduler.rescheduleLanguageFingerprinting(Joiner.on(Constants.LANG_CODE_DELIMITER).join(failedLanguages));

        } catch (InterruptedException | ExecutionException e) {
            LOG.error("Could not retrieve done fingerprinting jobs", e);
        }
    }
}

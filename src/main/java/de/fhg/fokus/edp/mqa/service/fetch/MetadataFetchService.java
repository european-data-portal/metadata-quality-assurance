package de.fhg.fokus.edp.mqa.service.fetch;

import com.google.common.base.Joiner;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.config.SystemConfiguration;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapData;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapDataClient;
import de.fhg.fokus.edp.mqa.service.dcatap.DcatapDataClientType;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.timer.MqaScheduler;
import org.slf4j.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.New;
import javax.inject.Inject;
import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

import static de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys.VALIDATION_LOG_FREQUENCY_IN_MINUTES;

/**
 * Created by bdi on 06/05/15.
 */
@Dependent
public class MetadataFetchService implements Serializable {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @DcatapData(DcatapDataClientType.CKAN)
    private DcatapDataClient dcatapDataClient;

    @Resource
    private ManagedExecutorService managedExecutorService;

    @Inject
    @New
    private Instance<CatalogTask> catalogTaskInstance;

    @Inject
    @MqaServiceConfig(VALIDATION_LOG_FREQUENCY_IN_MINUTES)
    private String logFrequencyInMinutes;

    @Inject
    private MqaHttpProvider mqaHttpProvider;

    @Inject
    private SystemConfiguration conf;

    @Inject
    private MqaScheduler mqaScheduler;

    private Set<CatalogTask> catalogTasks;

    /**
     * Fetches all catalogues from CKAN. Iterates through each catalogue and performs validation and distribution accessible checks on
     * each dataset and persists the results.
     */
    public void fetch() {
        catalogTasks = new HashSet<>();

        List<Catalog> catalogs = dcatapDataClient.getCatalogs();
        List<String> catalogsTitles = new ArrayList<>();

        catalogs.forEach(catalog -> {
            catalogsTitles.add(catalog.getTitle());
            CatalogTask task = catalogTaskInstance.get();
            task.setCatalogName(catalog.getName());
            catalogTasks.add(task);
        });

        ArrayList<CatalogTaskResponse> completedCatalogs = new ArrayList<>();

        Instant start = Instant.now();
        boolean isScheduled = false;

        ScheduledExecutorService logExecutor = Executors.newSingleThreadScheduledExecutor();
        Runnable logRunner = () -> {
            CatalogTaskResponse lastCatalog = completedCatalogs.get(completedCatalogs.size() - 1);
            long totalDatasets = 0;

            List<String> processedCatalogs = new ArrayList<>();
            List<String> remainingCatalogs = new ArrayList<>();
            catalogs.forEach(c -> remainingCatalogs.add(c.getName()));

            // iterate over all fetched catalogs to calculate stats
            for (CatalogTaskResponse catalog : completedCatalogs) {
                processedCatalogs.add(catalog.getCatalogName());
                totalDatasets += catalog.getDatasetCount();
                remainingCatalogs.remove(catalog.getCatalogName());
            }

            // log blocks of info
            LOG.info("Last catalog [{}] processed [{}] datasets in [{}]", lastCatalog.getCatalogName(), lastCatalog.getDatasetCount(), lastCatalog.getDuration());
            LOG.info("Processed catalogs: " + Joiner.on(", ").skipNulls().join(processedCatalogs));
            LOG.info("Processed datasets of completed catalogs: " + totalDatasets);
            LOG.info("Total time elapsed: " + Duration.between(start, Instant.now()));
            LOG.info("Catalogs remaining: " + Joiner.on(", ").skipNulls().join(remainingCatalogs));
        };

        LOG.info("Starting validation for the following catalogues: [{}]", catalogsTitles);
        CompletionService<CatalogTaskResponse> completionService = new ExecutorCompletionService<>(managedExecutorService);

        try {
            int remainingTasks = catalogTasks.size();
            catalogTasks.forEach(completionService::submit);

            while (remainingTasks > 0) {
                completedCatalogs.add(completionService.take().get());
                remainingTasks--;

                // start logging on first completed catalog
                if (!isScheduled) {
                    logExecutor.scheduleAtFixedRate(logRunner, 0, Integer.valueOf(logFrequencyInMinutes), TimeUnit.MINUTES);
                    isScheduled = true;
                }

                // Close connections
                mqaHttpProvider.getCm().closeExpiredConnections();
                mqaHttpProvider.getCm().closeIdleConnections(Integer.parseInt(conf.getProperties().getProperty
                        (MqaServiceConfigKeys.URL_CHECKER_CONNECTION_TIMEOUT)) * 1000, TimeUnit.SECONDS);
            }
        } catch (Exception e) {
            LOG.error("Error finishing catalog tasks", e);
        } finally {
//            mqaHttpProvider.triggerRefreshMetrics(); // pointless, as aync url checks are not done by the time this is triggered
            logRunner.run();
            logExecutor.shutdown();
        }
    }
}

package de.fhg.fokus.edp.mqa.service.persistence.entity;

import de.fhg.fokus.edp.mqa.service.model.DatasetSimilarity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bdi on 05/10/15.
 */
@Entity
@Table(name = "dataset_similarity")
@NamedQueries({
        @NamedQuery(name = "DatasetSimilarity.deleteAll", query = "DELETE FROM DatasetSimilarityEntity d"),
        @NamedQuery(name = "DatasetSimilarity.isExisting", query = "SELECT d FROM DatasetSimilarityEntity d WHERE d.datasetInstanceIdA = " +
                ":datasetInstanceIdA AND d.datasetInstanceIdB = :datasetInstanceIdB"),
        @NamedQuery(name = "DatasetSimilarity.listAllBeforeDate", query = "SELECT d FROM DatasetSimilarityEntity d WHERE d.dbUpdate < :date")
})
public class DatasetSimilarityEntity implements DatasetSimilarity, Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "dataset_instanceid_a", nullable = false)
    private String datasetInstanceIdA;

    @Column(name = "dataset_instanceid_b", nullable = false)
    private String datasetInstanceIdB;

    @Column(name = "score", nullable = false)
    private double score;

    @Column(name = "creation_date", nullable = false)
    private LocalDateTime creationDate;

    @Column(name = "db_update", nullable = false)
    private LocalDateTime dbUpdate;

    /**
     * Pre persist.
     */
    @PrePersist
    public void prePersist() {
        this.creationDate = LocalDateTime.now();
    }

    /**
     * Pre update.
     */
    @PreUpdate
    public void preUpdate() {
        this.dbUpdate = LocalDateTime.now();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getDatasetInstanceIdA() {
        return datasetInstanceIdA;
    }

    @Override
    public void setDatasetInstanceIdA(String datasetInstanceIdA) {
        this.datasetInstanceIdA = datasetInstanceIdA;
    }

    @Override
    public String getDatasetInstanceIdB() {
        return datasetInstanceIdB;
    }

    @Override
    public void setDatasetInstanceIdB(String datasetInstanceIdB) {
        this.datasetInstanceIdB = datasetInstanceIdB;
    }

    @Override
    public double getScore() {
        return score;
    }

    @Override
    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    @Override
    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public LocalDateTime getDbUpdate() {
        return dbUpdate;
    }

    @Override
    public void setDbUpdate(LocalDateTime dbUpdate) {
        this.dbUpdate = dbUpdate;
    }
}

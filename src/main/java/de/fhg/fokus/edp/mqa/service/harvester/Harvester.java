package de.fhg.fokus.edp.mqa.service.harvester;


import java.time.LocalDateTime;

/**
 * Created by bdi on 26/07/16.
 */
public class Harvester {
    private long id;
    private String name;
    private String frequency;
    private String script;
    private LocalDateTime scheduled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public LocalDateTime getScheduled() {
        return scheduled;
    }

    public void setScheduled(LocalDateTime scheduled) {
        this.scheduled = scheduled;
    }
}

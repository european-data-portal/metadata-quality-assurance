package de.fhg.fokus.edp.mqa.service.harvester;

import java.net.URL;

/**
 * Created by bdi on 22/07/16.
 */
public class Repository {

    private long id;
    private String name;
    private String type;
    private boolean incremental;
    private long sourceHarvester;
    private URL homepage;
    private String publisher;
    private String publisherEmail;
    private String language;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isIncremental() {
        return incremental;
    }

    public void setIncremental(boolean incremental) {
        this.incremental = incremental;
    }

    public long getSourceHarvester() {
        return sourceHarvester;
    }

    public void setSourceHarvester(long sourceHarvester) {
        this.sourceHarvester = sourceHarvester;
    }

    public URL getHomepage() {
        return homepage;
    }

    public void setHomepage(URL homepage) {
        this.homepage = homepage;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublisherEmail() {
        return publisherEmail;
    }

    public void setPublisherEmail(String publisherEmail) {
        this.publisherEmail = publisherEmail;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}

package de.fhg.fokus.edp.mqa.service.persistence.entity;

import de.fhg.fokus.edp.mqa.service.model.Publisher;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bdi on 18/04/15.
 */
@Entity
@Table(name = "publisher")
public class PublisherEntity implements Publisher {

    private static Logger LOG = LoggerFactory.getLogger(PublisherEntity.class);

    private static final int MAX_COLUMN_LENGTH_NAME = 255;
    private static final int MAX_COLUMN_LENGTH_EMAIL = 255;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @Column(name = "name", nullable = false, length = MAX_COLUMN_LENGTH_NAME)
    private String name;

    @Column(name = "email", nullable = false, length = MAX_COLUMN_LENGTH_EMAIL)
    private String email;

    @Column(name = "db_update", nullable = false)
    private LocalDateTime dbUpdate;

    /**
     * Update action.
     */
    @PrePersist
    @PreUpdate
    public void updateAction() {
        this.name = StringUtils.abbreviate(name, MAX_COLUMN_LENGTH_NAME);
        this.email = StringUtils.abbreviate(email, MAX_COLUMN_LENGTH_EMAIL);
    }

    @PreRemove
    public void tearDown(){
        LOG.debug("Publisher [{}] is going to be removed...", this.name);
    }

    @Override
    public LocalDateTime getDbUpdate() {
        return dbUpdate;
    }

    @Override
    public void setDbUpdate(LocalDateTime dbUpdate) {
        this.dbUpdate = dbUpdate;
    }

    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }
}

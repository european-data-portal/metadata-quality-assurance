package de.fhg.fokus.edp.mqa.service.persistence.entity;

import de.fhg.fokus.edp.mqa.service.model.Distribution;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

import static de.fhg.fokus.edp.mqa.service.config.Constants.ERROR_BAD_URL;
import static de.fhg.fokus.edp.mqa.service.config.Constants.OK;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bdi on 20/04/15.
 */
@Entity
@Table(name = "distribution")
@NamedQueries({
        @NamedQuery(name = "Distribution.findAll", query = "SELECT d FROM DistributionEntity d"),
        @NamedQuery(name = "Distribution.getAllCount", query = "SELECT COUNT(d) FROM DistributionEntity d"),
        @NamedQuery(name = "Distribution.findByInstanceId", query = "SELECT d FROM DistributionEntity d WHERE d.instanceId = :instanceId"),
        @NamedQuery(name = "Distribution.withDownloadUrlCount", query = "SELECT COUNT(d) FROM DistributionEntity d " +
                "WHERE d.downloadUrl IS NOT NULL AND d.downloadUrl <> '" + ERROR_BAD_URL + "'"),
        @NamedQuery(name = "Distribution.getAllOkAccessUrlsCount", query = "SELECT COUNT(d) FROM DistributionEntity d WHERE d.statusAccessUrl < 400"),
        @NamedQuery(name = "Distribution.getAllOkDownloadUrlsCount", query = "SELECT COUNT(d) FROM DistributionEntity d WHERE d.statusDownloadUrl < 400"),
        @NamedQuery(name = "Distribution.listStatusAccessCodes", query = "SELECT d.statusAccessUrl FROM DistributionEntity d WHERE d.statusAccessUrl > 0"),
        @NamedQuery(name = "Distribution.listStatusDownloadCodes", query = "SELECT d.statusDownloadUrl FROM DistributionEntity d WHERE d.statusDownloadUrl > 0"),
        @NamedQuery(name = "Distribution.statusCodesAccessCountByStatusCode", query = "SELECT COUNT(d) FROM DistributionEntity d WHERE d.statusAccessUrl = :status"),
        @NamedQuery(name = "Distribution.statusCodesDownloadCountByStatusCode", query = "SELECT COUNT(d) FROM DistributionEntity d WHERE d.statusDownloadUrl = :status"),
        @NamedQuery(name = "Distribution.listStatusAccessCodesByCatalog", query = "SELECT dist.statusAccessUrl, COUNT(dist) FROM CatalogEntity c " +
                "JOIN c.datasets d JOIN d.distributions dist WHERE dist.statusAccessUrl >= 400 AND c.instanceId = :instanceId GROUP BY dist.statusAccessUrl " +
                "ORDER BY COUNT(dist) DESC"),
        @NamedQuery(name = "Distribution.listStatusDownloadCodesByCatalog", query = "SELECT dist.statusDownloadUrl, COUNT(dist) FROM " +
                "CatalogEntity c JOIN c.datasets d JOIN d.distributions dist " +
                "WHERE dist.statusDownloadUrl >= 400 AND c.instanceId = :instanceId AND dist.statusDownloadUrl IS NOT NULL GROUP BY dist.statusDownloadUrl " +
                "ORDER BY COUNT(dist) DESC"),
        @NamedQuery(name = "Distribution.conformAndOk", query = "SELECT COUNT(d) FROM DistributionEntity d " +
                "WHERE d.statusAccessUrl < 400 AND (d.downloadUrl IS NULL OR d.statusDownloadUrl < 400)"),
        @NamedQuery(name = "Distribution.listAllInstanceIds", query = "SELECT d.instanceId FROM DistributionEntity d"),
        @NamedQuery(name = "Distribution.machineReadableCount", query = "SELECT COUNT(d) FROM DistributionEntity d WHERE d.machineReadable = TRUE"),
        @NamedQuery(name = "Distribution.listMachineReadableDistributions", query = "SELECT d FROM DistributionEntity d WHERE d.machineReadable = TRUE"),
        @NamedQuery(name = "Distribution.listUsedFormats", query = "SELECT DISTINCT(d.format), COUNT(d) FROM DistributionEntity d GROUP " +
                "BY d.format ORDER BY COUNT(d) DESC"),
        @NamedQuery(name = "Distribution.listUsedFormatsForCatalog", query = "SELECT DISTINCT(dist.format), COUNT(dist) FROM CatalogEntity c " +
                "JOIN c.datasets d JOIN d.distributions dist WHERE c.instanceId = :instanceId GROUP BY dist.format ORDER BY COUNT(dist) DESC"),
        @NamedQuery(name = "Distribution.countWithDownloadUrlByCatalog", query = "SELECT COUNT(dist) " +
                "FROM CatalogEntity c JOIN c.datasets d JOIN d.distributions dist WHERE c.instanceId = :instanceId AND dist.downloadUrl IS NOT NULL"),
        @NamedQuery(name = "Distribution.getMachineReadableDistributionsCountOfCatalog", query = "SELECT COUNT(dist) FROM CatalogEntity c " +
                "JOIN c.datasets d JOIN d.distributions dist WHERE dist.machineReadable = TRUE AND c.instanceId = :instanceId")
})
public class DistributionEntity implements Serializable, Distribution {

    private static Logger LOG = LoggerFactory.getLogger(DistributionEntity.class);

    @Transient
    private static final int MAX_COLUMN_LENGTH_URL = 8000;
    @Transient
    private static final int MAX_COLUMN_LENGTH_MESSAGE = 500;
    @Transient
    private static final int MAX_COLUMN_LENGTH_DEFAULT = 255;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @Column(name = "status_download_url")
    private int statusDownloadUrl;

    @Column(name = "status_access_url")
    private int statusAccessUrl;

    @Column(name = "download_url", length = MAX_COLUMN_LENGTH_URL)
    private String downloadUrl;

    @Column(name = "access_url", length = MAX_COLUMN_LENGTH_URL)
    private String accessUrl;

    @Column(name = "status_access_url_last_change_date")
    private LocalDateTime statusAccessUrlLastChangeDate;

    @Column(name = "status_download_url_last_change_date")
    private LocalDateTime statusDownloadUrlLastChangeDate;

    @Column(name = "instance_id", unique = true, nullable = false)
    private String instanceId;

    @Column(name = "db_update", nullable = false)
    private LocalDateTime dbUpdate;

    @Column(name = "access_error_message", length = MAX_COLUMN_LENGTH_MESSAGE)
    private String accessErrorMessage;

    @Column(name = "download_error_message", length = MAX_COLUMN_LENGTH_MESSAGE)
    private String downloadErrorMessage;

    @Column(name = "format", length = MAX_COLUMN_LENGTH_DEFAULT)
    private String format;

    @Column(name = "media_type", length = MAX_COLUMN_LENGTH_DEFAULT)
    private String mediaType;

    @Column(name = "media_type_checked", length = MAX_COLUMN_LENGTH_DEFAULT)
    private String mediaTypeChecked;

    @Column(name = "machine_readable", nullable = false)
    private boolean machineReadable;

    /**
     * Update action.
     */
    @PrePersist
    @PreUpdate
    public void updateAction() {
        if (statusAccessUrl == OK) {
            accessErrorMessage = "OK";
        }

        if (statusDownloadUrl == OK) {
            downloadErrorMessage = "OK";
        }

        this.accessErrorMessage = StringUtils.abbreviate(accessErrorMessage, MAX_COLUMN_LENGTH_MESSAGE);
        this.downloadErrorMessage = StringUtils.abbreviate(downloadErrorMessage, MAX_COLUMN_LENGTH_MESSAGE);
        this.downloadUrl = StringUtils.abbreviate(downloadUrl, MAX_COLUMN_LENGTH_URL);
        this.accessUrl = StringUtils.abbreviate(accessUrl, MAX_COLUMN_LENGTH_URL);
        this.mediaType = StringUtils.abbreviate(mediaType, MAX_COLUMN_LENGTH_DEFAULT);
        this.mediaTypeChecked = StringUtils.abbreviate(mediaTypeChecked, MAX_COLUMN_LENGTH_DEFAULT);
        this.dbUpdate = LocalDateTime.now();
        if (!StringUtils.isBlank(this.format)) {
            this.format = this.format.toUpperCase().trim();
            this.format = StringUtils.abbreviate(this.format, MAX_COLUMN_LENGTH_DEFAULT);
        }
    }

    @PreRemove
    public void tearDown(){
        LOG.debug("Distribution [{}] is going to be removed...", this.accessUrl);
    }
    @Override
    public LocalDateTime getDbUpdate() {
        return dbUpdate;
    }

    @Override
    public void setDbUpdate(LocalDateTime dbUpdate) {
        this.dbUpdate = dbUpdate;
    }

    @Override
    public String getInstanceId() {
        return instanceId;
    }

    @Override
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    @Override
    public LocalDateTime getStatusAccessUrlLastChangeDate() {
        return statusAccessUrlLastChangeDate;
    }

    @Override
    public void setStatusAccessUrlLastChangeDate(LocalDateTime statusAccessUrlLastChangeDate) {
        this.statusAccessUrlLastChangeDate = statusAccessUrlLastChangeDate;
    }

    @Override
    public LocalDateTime getStatusDownloadUrlLastChangeDate() {
        return statusDownloadUrlLastChangeDate;
    }

    @Override
    public void setStatusDownloadUrlLastChangeDate(LocalDateTime statusDownloadUrlLastChangeDate) {
        this.statusDownloadUrlLastChangeDate = statusDownloadUrlLastChangeDate;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public int getStatusDownloadUrl() {
        return statusDownloadUrl;
    }

    @Override
    public void setStatusDownloadUrl(int statusDownloadUrl) {
        this.statusDownloadUrl = statusDownloadUrl;
    }

    @Override
    public int getStatusAccessUrl() {
        return statusAccessUrl;
    }

    @Override
    public void setStatusAccessUrl(int statusAccessUrl) {
        this.statusAccessUrl = statusAccessUrl;
    }

    @Override
    public String getDownloadUrl() {
        return this.downloadUrl;
    }

    @Override
    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    @Override
    public String getAccessUrl() {
        return this.accessUrl;
    }

    @Override
    public void setAccessUrl(String accessUrl) {
        this.accessUrl = accessUrl;
    }

    @Override
    public String getAccessErrorMessage() {
        return accessErrorMessage;
    }

    @Override
    public void setAccessErrorMessage(String accessErrorMessage) {
        this.accessErrorMessage = accessErrorMessage;
    }

    @Override
    public String getDownloadErrorMessage() {
        return downloadErrorMessage;
    }

    @Override
    public void setDownloadErrorMessage(String downloadErrorMessage) {
        this.downloadErrorMessage = downloadErrorMessage;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String getMediaType() {
        return mediaType;
    }

    @Override
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    public String getMediaTypeChecked() {
        return mediaTypeChecked;
    }

    @Override
    public void setMediaTypeChecked(String mediaTypeChecked) {
        this.mediaTypeChecked = mediaTypeChecked;
    }

    @Override
    public boolean isMachineReadable() {
        return machineReadable;
    }

    @Override
    public void setMachineReadable(boolean machineReadable) {
        this.machineReadable = machineReadable;
    }
}

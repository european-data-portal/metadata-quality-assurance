package de.fhg.fokus.edp.mqa.service.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by bdi on 16/04/15.
 */
public interface Catalog extends Serializable, Comparable {

    /**
     * Gets id.
     *
     * @return the id
     */
    long getId();

    /**
     * Gets name.
     *
     * @return the name
     */
    String getName();

    /**
     * Sets name.
     *
     * @param name the name
     */
    void setName(String name);

    /**
     * Gets title.
     *
     * @return the title
     */
    String getTitle();

    /**
     * Sets title.
     *
     * @param title the title
     */
    void setTitle(String title);

    /**
     * Gets publisher.
     *
     * @return the publisher
     */
    Publisher getPublisher();

    /**
     * Sets publisher.
     *
     * @param publisher the publisher
     */
    void setPublisher(Publisher publisher);

    String getSpatial();
    void setSpatial(String spatial);

    String getDescription();
    void setDescription(String description);

    /**
     * Gets datasets.
     *
     * @return the datasets
     */
    List<Dataset> getDatasets();

    /**
     * Sets datasets.
     *
     * @param datasets the datasets
     */
    void setDatasets(List<Dataset> datasets);

    /**
     * Gets db update.
     *
     * @return the db update
     */
    LocalDateTime getDbUpdate();

    /**
     * Sets db update.
     *
     * @param dbUpdate the db update
     */
    void setDbUpdate(LocalDateTime dbUpdate);

    /**
     * Gets instance id.
     *
     * @return the instance id
     */
    String getInstanceId();

    /**
     * Sets instance id.
     *
     * @param instanceId the instance id
     */
    void setInstanceId(String instanceId);

}

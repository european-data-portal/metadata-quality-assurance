package de.fhg.fokus.edp.mqa.service.config;

import de.fhg.fokus.edp.mqa.service.log.Log;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

/**
 * Created by bdi on 09/04/15.
 */
@Stateless
public class MqaServiceConfigFactory {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private SystemConfiguration systemConfiguration;

    /**
     * Create config value string.
     *
     * @param injectionPoint the injection point
     * @return the string
     */
    @Produces
    @MqaServiceConfig(value = "", defaultValue = "")
    public String createConfigValue(InjectionPoint injectionPoint) {
        String key = injectionPoint.getAnnotated().getAnnotation(MqaServiceConfig.class).value();
        String defaultKey = injectionPoint.getAnnotated().getAnnotation(MqaServiceConfig.class).defaultValue();

        String value;

        if (key.isEmpty()) {
            value = systemConfiguration.getProperties().getProperty(defaultKey);
            LOG.info("Value for key {} is empty. Returning default value: {}", key, value);
        } else {
            value = systemConfiguration.getProperties().getProperty(key);
        }

        return value;
    }
}

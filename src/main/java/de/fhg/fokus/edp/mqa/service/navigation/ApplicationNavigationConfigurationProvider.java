package de.fhg.fokus.edp.mqa.service.navigation;

import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;

import javax.servlet.ServletContext;

/**
 * Created by bdi on 15/08/16.
 */
@RewriteConfiguration
public class ApplicationNavigationConfigurationProvider extends HttpConfigurationProvider {

    @Override
    public Configuration getConfiguration(ServletContext servletContext) {
        return ConfigurationBuilder.begin()
//                .addRule(TrailingSlash.remove())
                .addRule(Join.path("/{lang}").to("/dashboard.jsf"))
                .addRule(Join.path("/{lang}/catalogues").to("/catalogues.jsf"))
                .addRule(Join.path("/{lang}/reports").to("/reports.jsf"))
                .addRule(Join.path("/{lang}/downloadReport.html").to("/downloadReport.jsf"))
                .addRule(Join.path("/{lang}/catalogue/{catalogName}").to("/catalogDashboard.jsf"))
                .addRule(Join.path("/{lang}/catalogue/{catalogName}/violations").to("/violations.jsf"))
                .addRule(Join.path("/{lang}/catalogue/{catalogName}/distributions").to("/distributions.jsf"));
    }

    @Override
    public int priority() {
        return 0;
    }
}

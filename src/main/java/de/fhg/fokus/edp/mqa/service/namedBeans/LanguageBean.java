package de.fhg.fokus.edp.mqa.service.namedBeans;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by bdi on 17/06/15.
 */
@Named
@RequestScoped
public class LanguageBean implements Serializable {

    private static Map<String, Object> countries;

    static {
        countries = new LinkedHashMap<>();
        countries.put("English", Locale.ENGLISH);
        countries.put("Deutsch", Locale.GERMAN);
        countries.put("French", Locale.FRENCH);
    }

    private String localeCode;
    private String lang;
    private Locale locale;

    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        Map<String, String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String lang = params.get("lang");
        this.lang = lang;
        this.locale = new Locale(lang);
        this.localeCode = locale.getLanguage();

        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(lang));

    }

    /**
     * Country locale code changed.
     *
     * @param e the e
     * @throws IOException the io exception
     */
    public void countryLocaleCodeChanged(ValueChangeEvent e) throws IOException {

        this.lang = e.getNewValue().toString();
        this.localeCode = new Locale(lang).getLanguage();
        FacesContext.getCurrentInstance()
                .getViewRoot().setLocale(new Locale(lang));

        // loop country map to compare the locale code
        countries.entrySet().stream().filter(entry -> entry.getValue().toString().equals(lang)).forEach(entry -> FacesContext
                .getCurrentInstance()
                .getViewRoot().setLocale((Locale) entry.getValue()));

        // Build new URL for current site with new language as parameter. getViewId returns .xhtml site, but we need .html.
        String uri = String.format("%s%s", lang, FacesContext.getCurrentInstance().getViewRoot().getViewId().replace(".xhtml", ".html"));
        FacesContext.getCurrentInstance().getExternalContext().redirect(uri);
    }

    /**
     * Gets locale code.
     *
     * @return the locale code
     */
    public String getLocaleCode() {
        return localeCode;
    }

    /**
     * Sets locale code.
     *
     * @param localeCode the locale code
     */
    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    /**
     * Gets countries in map.
     *
     * @return the countries in map
     */
    public Map<String, Object> getCountriesInMap() {
        return countries;
    }

    /**
     * Gets lang.
     *
     * @return the lang
     */
    public String getLang() {
        return lang;
    }

    /**
     * Sets lang.
     *
     * @param lang the lang
     */
    public void setLang(String lang) {
        this.lang = lang;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}

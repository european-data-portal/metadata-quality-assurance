package de.fhg.fokus.edp.mqa.service.namedBeans.diagrams;

import de.fhg.fokus.edp.mqa.service.config.Constants;
import de.fhg.fokus.edp.mqa.service.namedBeans.ReportElementBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by bdi on 03/06/15.
 */
@Named
@RequestScoped
public class DashboardViolationsDiagramsBean extends DiagramBean implements Serializable {

    private Set<ReportElementBean> violationMostOccurredPieChart;
    private Set<ReportElementBean> violationsCountByCatalogBarChart;
    private Set<ReportElementBean> datasetsConformPieChart;

    @Override
    public boolean renderSection() {
        return getRenderMetric("/metric/global/render/violations");
    }

    @Override
    void createCharts() {
        createDatasetConformPercentage();
        createViolationMostOccurred();
        createViolationCountByCatalogBarChart();
    }

    private void createViolationCountByCatalogBarChart() {
        violationsCountByCatalogBarChart =
        getSetWithMultipleValuesSorted( "/metric/global/catalogues/compliance", Constants.BAR_CHART_ELEMENT_COUNT);
    }

    private void createDatasetConformPercentage() {
        datasetsConformPieChart = getSetWithBinaryValues("/metric/global/datasets/compliance");
    }

    private void createViolationMostOccurred() {
        violationMostOccurredPieChart = getSetWithFixedKeysAndMultipleValues("/metric/global/datasets/violations");
    }

    /**
     * Gets violation most occurred pie chart.
     *
     * @return the violation most occurred pie chart
     */
    public Set<ReportElementBean> getViolationMostOccurredPieChart() {
        return violationMostOccurredPieChart;
    }

    /**
     * Sets violation most occurred pie chart.
     *
     * @param violationMostOccurredPieChart the violation most occurred pie chart
     */
    public void setViolationMostOccurredPieChart(Set<ReportElementBean> violationMostOccurredPieChart) {
        this.violationMostOccurredPieChart = violationMostOccurredPieChart;
    }

    /**
     * Gets violations count by catalogId bar chart.
     *
     * @return the violations count by catalogId bar chart
     */
    public Set<ReportElementBean> getViolationsCountByCatalogBarChart() {
        return violationsCountByCatalogBarChart;
    }

    /**
     * Sets violations count by catalogId bar chart.
     *
     * @param violationsCountByCatalogBarChart the violations count by catalogId bar chart
     */
    public void setViolationsCountByCatalogBarChart(Set<ReportElementBean> violationsCountByCatalogBarChart) {
        this.violationsCountByCatalogBarChart = violationsCountByCatalogBarChart;
    }

    /**
     * Gets distribution conform pie chart.
     *
     * @return the distribution conform pie chart
     */
    public Set<ReportElementBean> getDatasetsConformPieChart() {
        return datasetsConformPieChart;
    }

    /**
     * Sets distribution conform pie chart.
     *
     * @param datasetsConformPieChart the distribution conform pie chart
     */
    public void setDatasetsConformPieChart(Set<ReportElementBean> datasetsConformPieChart) {
        this.datasetsConformPieChart = datasetsConformPieChart;
    }
}

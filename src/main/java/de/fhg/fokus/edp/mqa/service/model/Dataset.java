package de.fhg.fokus.edp.mqa.service.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Created by bdi on 13/04/15.
 */
public interface Dataset extends Serializable {

    /**
     * Gets id.
     *
     * @return the id
     */
    Long getId();

    /**
     * Gets name.
     *
     * @return the name
     */
    String getName();

    /**
     * Gets instance id.
     *
     * @return the instance id
     */
    String getInstanceId();

    /**
     * Sets instance id.
     *
     * @param instanceId the instance id
     */
    void setInstanceId(String instanceId);

    /**
     * Sets name.
     *
     * @param name the name
     */
    void setName(String name);

    /**
     * Gets title.
     *
     * @return the title
     */
    String getTitle();

    /**
     * Sets title.
     *
     * @param title the title
     */
    void setTitle(String title);

    /**
     * Gets distributions.
     *
     * @return the distributions
     */
    Set<Distribution> getDistributions();

    /**
     * Sets distributions.
     *
     * @param distributions the distributions
     */
    void setDistributions(Set<Distribution> distributions);

    /**
     * Gets violations.
     *
     * @return the violations
     */
    Set<Violation> getViolations();

    /**
     * Sets violations.
     *
     * @param violations the violations
     */
    void setViolations(Set<Violation> violations);

    /**
     * Gets db update.
     *
     * @return the db update
     */
    LocalDateTime getDbUpdate();

    /**
     * Sets db update.
     *
     * @param dbUpdate the db update
     */
    void setDbUpdate(LocalDateTime dbUpdate);

    boolean isMachineReadable();

    void setMachineReadable(boolean machineReadable);

    /**
     * Gets licence id.
     *
     * @return the licence id
     */
    String getLicenceId();

    /**
     * Sets licence id.
     *
     * @param licenceId the licence id
     */
    void setLicenceId(String licenceId);
}

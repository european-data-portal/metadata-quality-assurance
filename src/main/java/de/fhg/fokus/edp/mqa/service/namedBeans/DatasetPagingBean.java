package de.fhg.fokus.edp.mqa.service.namedBeans;

import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Distribution;
import de.fhg.fokus.edp.mqa.service.model.Violation;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by bdi on 07/06/15.
 */
@Named
@RequestScoped
public class DatasetPagingBean implements Serializable {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    private CurrentCatalogBean currentCatalogBean;

    private String catalogName;
    private List<Dataset> dataList;
    private int totalRows;
    private int firstRow;
    private int rowsPerPage;
    private int totalPages;
    private int pageRange;
    private Integer[] pages;
    private int currentPage;

    /**
     * Instantiates a new Catalog paging bean.
     */
    @PostConstruct
    public void init() {
        rowsPerPage = 10;
        pageRange = 6;

        Map<String, String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        if (params.get("page") != null) {
            try {
                page((Integer.valueOf(params.get("page")) - 1) * rowsPerPage);
            } catch (NumberFormatException e) {
                LOG.error("Error parsing page param value [{}]", params.get("page"));
            }
        }
    }

    private void page(int firstRow) {
        this.firstRow = firstRow;
        loadDataList(); // Load requested page.
    }

    private void loadDataList() {
        catalogName = currentCatalogBean.getCatalogName();

        switch (FacesContext.getCurrentInstance().getViewRoot().getViewId()) {
            case "/distributions.xhtml":
                dataList = vdc.listDatasetsWithStatusCodesNotOkByCatalogName(firstRow, rowsPerPage, catalogName);
                dataList.forEach(dataset -> {
                    Set<Distribution> distributions = new HashSet<>(vdc.listDistributionsByDatasetName(dataset.getName()));
                    dataset.setDistributions(distributions);
                });
                totalRows = currentCatalogBean.getAffectedDatasetsWithDistributionIssues();
                LOG.debug("Data list has [{}] non accessible datasets", totalRows);
                break;
            case "/violations.xhtml":
                dataList = vdc.listDatasetsNonConformByCatalogName(firstRow, rowsPerPage, catalogName);
                dataList.forEach(dataset -> {
                    Set<Violation> violations = new HashSet<>(vdc.listViolationsByDatasetName(dataset.getName()));
                    dataset.setViolations(violations);
                });
                totalRows = currentCatalogBean.getAffectedDatasetsWithViolationIssues();
                LOG.debug("Data list has [{}] non conformant datasets", totalRows);
                break;
        }

        // Set currentPage, totalPages and pages.
        currentPage = (totalRows / rowsPerPage) - ((totalRows - firstRow) / rowsPerPage) + 1;
        totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
        int pagesLength = Math.min(pageRange, totalPages);
        pages = new Integer[pagesLength];

        // firstPage must be greater than 0 and lesser than totalPages-pageLength.
        int firstPage = Math.min(Math.max(0, currentPage - (pageRange / 2)), totalPages - pagesLength);

        // Create pages (page numbers for page links).
        for (int i = 0; i < pagesLength; i++) {
            pages[i] = ++firstPage;
        }
    }

    /**
     * Gets data list.
     *
     * @return the data list
     */
    public List<Dataset> getDataList() {
        if (dataList == null) {
            loadDataList();
        }
        return dataList;
    }

    /**
     * Sets data list.
     *
     * @param dataList the data list
     */
    public void setDataList(List<Dataset> dataList) {
        this.dataList = dataList;
    }

    /**
     * Gets total rows.
     *
     * @return the total rows
     */
    public int getTotalRows() {
        return totalRows;
    }

    /**
     * Sets total rows.
     *
     * @param totalRows the total rows
     */
    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    /**
     * Gets first row.
     *
     * @return the first row
     */
    public int getFirstRow() {
        return firstRow;
    }

    /**
     * Sets first row.
     *
     * @param firstRow the first row
     */
    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    /**
     * Gets rows per page.
     *
     * @return the rows per page
     */
    public int getRowsPerPage() {
        return rowsPerPage;
    }

    /**
     * Sets rows per page.
     *
     * @param rowsPerPage the rows per page
     */
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    /**
     * Gets total pages.
     *
     * @return the total pages
     */
    public int getTotalPages() {
        return totalPages;
    }

    /**
     * Sets total pages.
     *
     * @param totalPages the total pages
     */
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * Get pages integer [ ].
     *
     * @return the integer [ ]
     */
    public Integer[] getPages() {
        return pages;
    }

    /**
     * Sets pages.
     *
     * @param pages the pages
     */
    public void setPages(Integer[] pages) {
        this.pages = pages;
    }

    /**
     * Gets current page.
     *
     * @return the current page
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * Sets current page.
     *
     * @param currentPage the current page
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * Gets catalog name.
     *
     * @return the catalog name
     */
    public String getCatalogName() {
        return catalogName;
    }

}

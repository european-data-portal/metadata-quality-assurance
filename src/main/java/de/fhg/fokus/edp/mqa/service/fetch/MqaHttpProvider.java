package de.fhg.fokus.edp.mqa.service.fetch;

import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.config.SystemConfiguration;
import de.fhg.fokus.edp.mqa.service.log.Log;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.Optional;

/**
 * Created by bdi on 23/07/15.
 */
@ApplicationScoped
public class MqaHttpProvider implements Serializable {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    private SystemConfiguration conf;

    private PoolingHttpClientConnectionManager cm;

    private CloseableHttpClient httpClient;

    /**
     * Get method in order to receive the current connection manager.
     *
     * @return The current connection manager
     */
    PoolingHttpClientConnectionManager getCm() {
        return cm;
    }

    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(500);
        cm.setDefaultMaxPerRoute(100);

        httpClient = HttpClients.custom().setConnectionManager(cm).build();
    }

    @PreDestroy
    public void tearDown() {
        try {
            httpClient.close();
        } catch (IOException e) {
            LOG.warn("Failed to close httpClient: " + e.getMessage());
        }
    }

    void sendUrlCheckPost(String distributionId, String jsonBody) {
        try {
            HttpPost postRequest = new HttpPost(conf.getProperties().getProperty(MqaServiceConfigKeys.URL_CHECK_ENDPOINT));
            postRequest.addHeader("Content-Type", "application/json");

            StringEntity body = new StringEntity(jsonBody);
            postRequest.setEntity(body);

            ResponseHandler<StatusLine> responseHandler = HttpResponse::getStatusLine;
            StatusLine status = httpClient.execute(postRequest, responseHandler);

            if (status.getStatusCode() != 202)
                LOG.warn("URL check request for distribution with ID [{}] rejected by endpoint: {} ({}). Request: {}",
                        distributionId, status.getReasonPhrase(), status.getStatusCode(), jsonBody);

        } catch (IOException e) {
            LOG.warn("Exception thrown while sending url check request for distribution with ID [{}]: {}", distributionId, e.getMessage());
        }
    }

    public Optional<JsonObject> getMetric(String endpoint) {
        String url = conf.getProperties().getProperty(MqaServiceConfigKeys.METRIC_CACHE_URL) + endpoint;
        HttpGet request = new HttpGet(url);

        try {
            HttpResponse response = httpClient.execute(request);

            if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() < 400) {
                try (InputStream is = response.getEntity().getContent()) {
                    JsonObject result = Json.createReader(is).readObject();
                    LOG.debug("Successfully loaded metric from [{}]: {}", url, result.toString());
                    return Optional.of(result);
                }
            } else {
                LOG.error("Retrieving metric from [{}] returned status [{}]", url, response.getStatusLine().getStatusCode());
                return Optional.empty();
            }
        } catch (IOException e) {
            LOG.error("Failed to retrieve metric from [{}]: ", url, e.getMessage());
            return Optional.empty();
        }
    }

    public JsonObject getRemoteFormats(URL formatsUrl) {

        JsonObject formats;

        CloseableHttpClient remoteFormatClient = HttpClients.custom()
                .setConnectionManager(cm)
                .setRedirectStrategy(new LaxRedirectStrategy())
                .setRetryHandler(new DefaultHttpRequestRetryHandler(1, false))
                .build();

        HttpGet getRequest = new HttpGet(formatsUrl.toString());

        try (CloseableHttpResponse response = remoteFormatClient.execute(getRequest, HttpClientContext.create())) {
            JsonReader jReader = Json.createReader(response.getEntity().getContent());
            formats = jReader.readObject();

        } catch (Exception e) {
            LOG.error("Json formats are not set because of: {}", e.getMessage());
            formats = null;
        }

        return formats;
    }

    void triggerRefreshMetrics() {
        try {
            HttpPost postRequest = new HttpPost(conf.getProperties().getProperty(MqaServiceConfigKeys.METRIC_CACHE_URL) + "/admin/refresh/global");
            ResponseHandler<StatusLine> responseHandler = HttpResponse::getStatusLine;
            StatusLine status = httpClient.execute(postRequest, responseHandler);

            if (status.getStatusCode() != 202)
                LOG.warn("Triggering refresh of global metrics rejected by endpoint: {} ({})", status.getReasonPhrase(), status.getStatusCode());


            postRequest = new HttpPost(conf.getProperties().getProperty(MqaServiceConfigKeys.METRIC_CACHE_URL) + "/admin/refresh/catalogue");
            responseHandler = HttpResponse::getStatusLine;
            status = httpClient.execute(postRequest, responseHandler);

            if (status.getStatusCode() != 202)
                LOG.warn("Triggering refresh of catalogue metrics rejected by endpoint: {} ({})", status.getReasonPhrase(), status.getStatusCode());

        } catch (IOException e) {
            LOG.warn("Exception thrown while sending refresh metrics request: {}", e.getMessage());
        }
    }

    public JsonArray getKnownLicences() throws IOException {
        HttpGet getLicences = new HttpGet("https://www.europeandataportal.eu/licensing-assistant/api/ckan.json");

        try (CloseableHttpResponse response = httpClient.execute(getLicences, HttpClientContext.create())) {
            JsonReader jsonReader = Json.createReader(response.getEntity().getContent());
            return jsonReader.readArray();
        }
    }
}

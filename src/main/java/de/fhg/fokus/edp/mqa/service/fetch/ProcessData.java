package de.fhg.fokus.edp.mqa.service.fetch;

import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Catalog;
import de.fhg.fokus.edp.mqa.service.model.Dataset;
import de.fhg.fokus.edp.mqa.service.model.Distribution;
import de.fhg.fokus.edp.mqa.service.model.Violation;
import de.fhg.fokus.edp.mqa.service.utils.Formats;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import org.slf4j.Logger;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bdi on 08/01/16.
 */
@Dependent
public class ProcessData implements Serializable {

    @Inject
    @Log
    private Logger LOG;

    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    /**
     * Process datasets list.
     *
     * @param catalogName    the catalog name
     * @param remoteDatasets the remote datasets
     * @return the list
     */
    ProcessDataResponse processDatasets(String catalogName, List<Dataset> remoteDatasets) {
        List<Dataset> persistentDatasets = new ArrayList<>();

        int newDatasetCount = 0;
        int updatedDatasetCount = 0;

        final Catalog catalog = vdc.getCatalogByName(catalogName);
        Dataset persistentDataset;

        for (Dataset remoteDataset : remoteDatasets) {
            try {
                persistentDataset = vdc.getDatasetByName(remoteDataset.getName());

                if (persistentDataset != null) {
                    LOG.trace("Dataset [{}] already exists -> update", remoteDataset.getName());
                    persistentDataset.setName(remoteDataset.getName());
                    persistentDataset.setTitle(remoteDataset.getTitle());

                    // collect violations, persist them and add all to current dataset
                    persistentDataset.getViolations().clear();
                    persistentDataset.getDistributions().clear();
                    persistentDataset = vdc.updateEntity(persistentDataset);

                    for (Violation violation : remoteDataset.getViolations())
                        persistentDataset.getViolations().add(vdc.persistEntity(violation));

                    for (Distribution distribution : remoteDataset.getDistributions())
                        persistentDataset.getDistributions().add(vdc.persistEntity(distribution));

                    persistentDataset.setDbUpdate(LocalDateTime.now());
                    persistentDataset = vdc.updateEntity(persistentDataset);
                    updatedDatasetCount++;
                } else {
                    try {
                        persistentDataset = vdc.persistEntity(remoteDataset);
                        catalog.getDatasets().add(persistentDataset);
                        newDatasetCount++;
                    } catch (Exception ex) {
                        LOG.error("Cannot persist dataset [{}]", remoteDataset.getName(), ex);
                        continue;
                    }
                }
            } catch (Exception e) {
                LOG.error("Cannot update dataset [{}]", remoteDataset.getName(), e);
                continue;
            }

            catalog.setDbUpdate(LocalDateTime.now());
            vdc.updateEntity(catalog);
            persistentDatasets.add(persistentDataset);
        }

        return new ProcessDataResponse(persistentDatasets, newDatasetCount, updatedDatasetCount);
    }

    class ProcessDataResponse {

        private List<Dataset> datasets;
        private int newDatasetCount;
        private int updatedDatasetCount;

        public ProcessDataResponse(List<Dataset> datasets, int newDatasetCount, int updatedDatasetCount) {
            this.datasets = datasets;
            this.newDatasetCount = newDatasetCount;
            this.updatedDatasetCount = updatedDatasetCount;
        }

        public List<Dataset> getDatasets() {
            return datasets;
        }

        public void setDatasets(List<Dataset> datasets) {
            this.datasets = datasets;
        }

        public int getNewDatasetCount() {
            return newDatasetCount;
        }

        public void setNewDatasetCount(int newDatasetCount) {
            this.newDatasetCount = newDatasetCount;
        }

        public int getUpdatedDatasetCount() {
            return updatedDatasetCount;
        }

        public void setUpdatedDatasetCount(int updatedDatasetCount) {
            this.updatedDatasetCount = updatedDatasetCount;
        }
    }
}

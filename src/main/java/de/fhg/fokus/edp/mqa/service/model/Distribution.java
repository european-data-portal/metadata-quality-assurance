package de.fhg.fokus.edp.mqa.service.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by bdi on 16/04/15.
 */
public interface Distribution extends Serializable {

    /**
     * Gets id.
     *
     * @return the id
     */
    long getId();

    /**
     * Gets status download url.
     *
     * @return the status download url
     */
    int getStatusDownloadUrl();

    /**
     * Sets status download url.
     *
     * @param statusDownloadUrl the status download url
     */
    void setStatusDownloadUrl(int statusDownloadUrl);

    /**
     * Gets status download url last change date.
     *
     * @return the status download url last change date
     */
    LocalDateTime getStatusDownloadUrlLastChangeDate();

    /**
     * Sets status download url last change date.
     *
     * @param date the date
     */
    void setStatusDownloadUrlLastChangeDate(LocalDateTime date);

    /**
     * Gets status access url.
     *
     * @return the status access url
     */
    int getStatusAccessUrl();

    /**
     * Sets status access url.
     *
     * @param statusAccessUrl the status access url
     */
    void setStatusAccessUrl(int statusAccessUrl);

    /**
     * Gets status access url last change date.
     *
     * @return the status access url last change date
     */
    LocalDateTime getStatusAccessUrlLastChangeDate();

    /**
     * Sets status access url last change date.
     *
     * @param date the date
     */
    void setStatusAccessUrlLastChangeDate(LocalDateTime date);

    /**
     * Gets download url.
     *
     * @return the download url
     */
    String getDownloadUrl();

    /**
     * Sets download url.
     *
     * @param downloadUrl the download url
     */
    void setDownloadUrl(String downloadUrl);

    /**
     * Gets access url.
     *
     * @return the access url
     */
    String getAccessUrl();

    /**
     * Sets access url.
     *
     * @param accessUrl the access url
     */
    void setAccessUrl(String accessUrl);

    /**
     * Gets instance id.
     *
     * @return the instance id
     */
    String getInstanceId();

    /**
     * Sets instance id.
     *
     * @param instanceId the instance id
     */
    void setInstanceId(String instanceId);

    /**
     * Gets db update.
     *
     * @return the db update
     */
    LocalDateTime getDbUpdate();

    /**
     * Sets db update.
     *
     * @param dbUpdate the db update
     */
    void setDbUpdate(LocalDateTime dbUpdate);

    /**
     * Gets access error message.
     *
     * @return the access error message
     */
    String getAccessErrorMessage();

    /**
     * Sets access error message.
     *
     * @param accessErrorMessage the access error message
     */
    void setAccessErrorMessage(String accessErrorMessage);

    /**
     * Gets download error message.
     *
     * @return the download error message
     */
    String getDownloadErrorMessage();

    /**
     * Sets download error message.
     *
     * @param DownloadErrorMessage the download error message
     */
    void setDownloadErrorMessage(String DownloadErrorMessage);

    /**
     * Gets format.
     *
     * @return the format
     */
    String getFormat();

    /**
     * Sets format.
     *
     * @param format the format
     */
    void setFormat(String format);

    /**
     * Gets media type.
     *
     * @return the media type
     */
    String getMediaType();

    /**
     * Sets media type.
     *
     * @param mediaType the media type
     */
    void setMediaType(String mediaType);

    /**
     * Gets media type checked.
     *
     * @return the media type checked
     */
    String getMediaTypeChecked();

    /**
     * Sets media type checked.
     *
     * @param mediaType the media type
     */
    void setMediaTypeChecked(String mediaType);

    /**
     * Is machine readable boolean.
     *
     * @return the boolean
     */
    boolean isMachineReadable();

    /**
     * Sets machine readable.
     *
     * @param machineReadable the machine readable
     */
    void setMachineReadable(boolean machineReadable);
    
}

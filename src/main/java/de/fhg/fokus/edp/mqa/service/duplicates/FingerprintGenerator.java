package de.fhg.fokus.edp.mqa.service.duplicates;

import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.config.SystemConfiguration;
import de.fhg.fokus.edp.mqa.service.log.Log;
import org.slf4j.Logger;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


/**
 * Class to generate fingerprints for EDP datasets.
 * The fingerprints are locality-sensitive hash values (TLSH)
 * made from each dataset's title and description.
 *
 * Organisation of output files:
 * Most of the EDP catalogs (ca. 70+) have a dct:spatial property,
 * a two-uppercase-character language code;
 * some catalogs have not; they are called "pan-European".
 *
 * The invocation of the generation program is parametrized
 * with language code(s) and/or "__" for pan-European;
 * for each such, a dedicated output file is created,
 * with the code as part of the name,
 * and the fingerprints of all datasets within the catalogs
 * with that language code are written to that file.
 * 
 */
@Dependent
public class FingerprintGenerator implements Serializable {

	@Inject
	@Log
	private Logger LOG;

	@Inject
	@MqaServiceConfig(MqaServiceConfigKeys.FP_REMOTE_URL)
	private String fingerprintRemoteUrl;

	@Inject
	@MqaServiceConfig(MqaServiceConfigKeys.FP_DIRECTORY)
	private String fingerprintBaseDir;

	@Inject
	@MqaServiceConfig(MqaServiceConfigKeys.CKAN_BASICAUTH_USERNAME)
	private String ckanUsername;

	@Inject
	@MqaServiceConfig(MqaServiceConfigKeys.CKAN_BASICAUTH_PASSWORD)
	private String ckanPassword;

	/**
	 * Main call, usage: java genFP <one or more two-char language codes or __ for pan-European >
	 * Not reentrant within same class instance.
	 */
	Path fingerprint(String langCode) throws LanguageFailedException {
		Path outputFile = Paths.get(fingerprintBaseDir + File.separator + langCode + ".fp");
		Path tmpFile = Paths.get(outputFile + ".tmp");
		LOG.debug("Fingerprinting language [{}] to file [{}]", langCode, outputFile.toAbsolutePath());

		Instant start = Instant.now();

		try {
			Files.createDirectories(outputFile.getParent());

			try (BufferedWriter bw = Files.newBufferedWriter(tmpFile, Charset.forName("utf-8"))) {

				for (String catUri : getCatalogURIs(langCode)) {
					LOG.debug("Processing [{}] catalog {}", langCode, catUri);
					processCatalog(catUri, bw, langCode);
				}

				LOG.info("Finished fingerprinting language [{}] in [{}]", langCode, Duration.between(start, Instant.now()));
				Files.move(tmpFile, outputFile, REPLACE_EXISTING);
				return outputFile;
			} catch (IOException e) {
				LOG.error("Failed to write file [{}]", outputFile.toAbsolutePath(), e);
				throw new LanguageFailedException(langCode);
			}

		} catch (IOException e) {
			LOG.error("Could not create directory [{}]", outputFile.getParent(), e);
			throw  new LanguageFailedException(langCode);
		} finally {
			// clean up temp file
			try {
				if (Files.exists(tmpFile))
					Files.delete(tmpFile);
			} catch (IOException e) {
				LOG.error("Failed to clean fingerprinting temp file [{}]", tmpFile.toAbsolutePath(), e);
			}
		}
	}

	/**
	 * Generate fingerprints for one particular EDP catalog.
	 * @param pcatUri Catalog URI.
	 * @param pwb Buffered writer for the results.
	 */
	private void processCatalog(String pcatUri, BufferedWriter pwb, String langCode) throws LanguageFailedException {

		int offset = 0, limit = 1024, rxlines;
		StringBuilder strbHiFi = new StringBuilder(), strbFiltered = new StringBuilder();
		int[] fingerprint = new int[NBuckets];

		try {
			do {
				BufferedReader bstrrd = issueEdpSparqlQuery(buildEDPquery(pcatUri, offset, limit));
				//~ print query: pbw.write(strbHiFi.toString()); pbw.newLine();

				if (bstrrd == null)
					throw new LanguageFailedException(langCode);

				bstrrd.readLine();    // skip header
				int peekch = bstrrd.read();
				rxlines = 0;

				while (true) {
					while (peekch != -1 && Character.isWhitespace(peekch)) {
						peekch = bstrrd.read();
					}

					if (peekch == -1)
						break; //EOF

					peekch = scanString(peekch, bstrrd, strbHiFi, strbFiltered);
					String url = strbHiFi.toString();
					strbFiltered.setLength(0);

					peekch = scanString(peekch, bstrrd, strbHiFi, strbFiltered);
					int hflength = strbHiFi.length();
					strbFiltered.append("    "); // ensure separation of TLSH computation of title and description

					peekch = scanString(peekch, bstrrd, strbHiFi, strbFiltered);

					rxlines++;
					hflength += strbHiFi.length();
					String text = strbFiltered.toString();
					//~ System.out.println("¶ " + url + "  ¶¶ " + text + "\n¶¶¶ " + strbHiFi.toString());

					pwb.write('"');
					pwb.write(url);
					pwb.write("\" \"");
					TLSHfingerprint(text, fingerprint);

					for (int i = 0; i < NBuckets; i += 2)
						pwb.write("0123456789ABCDEF".charAt(fingerprint[i] * 4 + fingerprint[i + 1]));

					pwb.write("\" " + hflength);
					pwb.newLine();
				}
				offset += limit;
			} while (rxlines > 0);
		} catch (IOException e) {
			LOG.error("Processing catalog [{}] failed", pcatUri, e);
			throw new LanguageFailedException(langCode);
		}
	}


	/**
	 * Scan string from peekch and reader pbur.
	 * Double quotes are converted to single quotes;
	 * no other escaping is done.
	 * The scanned string is stored into two stringbuilders,
	 * firstly in its original forms,
	 * and secondly converted to lower case, stripped from punctuation, etc.
	 * (stemming is planned for the future).
	 * The first stringbuilder is cleared on entry,
	 * the second is not, in order to permit concatenations of successive results.
	 * An immediately following comma is consumed too.
	 * @param peekch first character (which may be opening quote or preceding white space), already read from pbur.
	 * @return first unconsumed character (already rad from pbur).
	 */
	private int scanString(int peekch, BufferedReader pbur, StringBuilder strbHiFi, StringBuilder strbFiltered) throws LanguageFailedException {

	    try {
            while (peekch != -1 && Character.isWhitespace(peekch)) {
                peekch = pbur.read();
            }

            if (peekch == '"') {
                peekch = pbur.read();
            } else {
                throw new IllegalArgumentException("+++ opening string quote expected");
            }

            final boolean foldcase = false;
            int lastspacepos = strbFiltered.length() - 1;

            strbHiFi.setLength(0);

            do {
                //~ ch = pbur.read ();
                if (peekch == -1)
                    throw new IllegalArgumentException("+++ END OF STREAM in \"string…");

                char cch = (char) peekch;

                if (Character.isUpperCase(cch)) {
                    strbHiFi.append(foldcase ? Character.toLowerCase(cch) : cch);
                    strbFiltered.append(Character.toLowerCase(cch));
                } else if (Character.isLetterOrDigit(cch)) {
                    strbHiFi.append(cch);
                    strbFiltered.append(cch);
                } else if ((peekch == '\'' || peekch == '_') && lastspacepos < strbFiltered.length() - 1) { // preserve apostrophes and underscores not at beginning of word:
                    strbHiFi.append(cch);
                    strbFiltered.append(cch);
                } else {
                    // Console.WriteLine(strbFiltered.ToString() + "««« " + lastspacepos);
                    if (strbFiltered.length() > 0 && strbFiltered.charAt(strbFiltered.length() - 1) != ' ') { // avoid redundant spaces
                        //~ String wrd = strbFiltered.substring(lastspacepos+1, strbFiltered.length()).toLowerCase();
                        //					if (w.Contains(" ")) Console.WriteLine("+++ space in :" +w);

                        String wrd = strbFiltered.substring(lastspacepos + 1).toLowerCase();

                        if (EnglishStopWordsL1.contains(wrd)) {
                            strbFiltered.setLength(lastspacepos + 1);    // remove stopword by truncating stringbuilder
                        } else {
//						if (fillFiltered && w!="" && Char.IsLetter(w[0]) && !EnglishStopWordsL2.Contains(w)) {
//							var wstemmed = stemmer.Stem(w);
//							wwr.WriteLine(w + "\t" + wstemmed+ "\t" + wstemmed.GetHashCode());	
//							stemmedWordHashvalues.Add((ushort)(Math.Abs(wstemmed.GetHashCode())%65519));
//						}
                            strbFiltered.append(' ');
                            lastspacepos = strbFiltered.length() - 1;
//						wordCount++;
                        }
                    }

                    if (peekch == '"') {
                        peekch = pbur.read();

                        if (peekch != '"')
                            break;    // string finished
                        //~ else
                        //~ pbur.read ();	// string continues, consume second quote
                    }
                    strbHiFi.append(cch);
                }

                peekch = pbur.read();
            } while (true);

            if (strbFiltered.length() > 0 && strbFiltered.charAt(strbFiltered.length() - 1) == ' ')
                strbFiltered.setLength(strbFiltered.length() - 1);    // trim trailing space from strbFiltered

            if (peekch == ',')
                peekch = pbur.read();    // consume csv comma if any
        } catch (IOException | IllegalArgumentException e) {
	        LOG.error("Failed to scan String", e);
	        throw new LanguageFailedException();
        }

		return peekch;
	}
	
	/**
	 * builds a SPARQL query
	 * @param pcatUri The EDP data catalog URI whose datasets are to be queried.
	 */
	private String buildEDPquery(String pcatUri, int poffset, int plimit) {
		StringBuilder sb = new StringBuilder();

		sb.append("select ?s ?t ?d where {\n");
		sb.append(	"\t{ select ?s where { SELECT ?s WHERE { <").append(pcatUri).
				append(	"> dcat:dataset ?s } ORDER BY asc(?s) } ");

		if (plimit > 0)
			sb.append(" LIMIT ").append(plimit);

		sb.append(" OFFSET ").append(poffset);
		sb.append("}\n\t?s <http://purl.org/dc/terms/title> ?t;").
			append("<http://purl.org/dc/terms/description> ?d.\n");
		sb.append("\tFILTER (lang(?t)=\"\" && lang(?d)=\"\")}");

		return sb.toString();
	}

    /**
     * Submits the query to the SPARQL interface of the EDP.
     * URL encoding of query is taken care of here.
     * In reply returned, header line is NOT skipped.
     * @param pquery SPARQL query
     * @return unprocessed reply in BufferedReader.
     */
    private BufferedReader issueEdpSparqlQuery(String pquery) {

    	int tries = 3;

    	while (tries > 0) {
			try {
				String url = fingerprintRemoteUrl + "/sparql?default-graph-uri=&query="
						+ java.net.URLEncoder.encode(pquery, "UTF-8")        //in C#: WebUtility.UrlEncode
						+ "&format=text%2Fcsv&timeout=10000&debug=off";
				LOG.debug("SPARQL query is [{}]", url);

				URL obj = new URL(url);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();

				// add authentication
				if (ckanUsername != null && ckanPassword != null) {
					String headerAuth = String.format("Basic %s", Base64.getEncoder().encodeToString(String.format("%s:%s", ckanUsername, ckanPassword).getBytes()));
					con.setRequestProperty("authorization", headerAuth);
				}

				// optional default is GET
				con.setRequestMethod("GET");

				// give it 15 seconds to respond
				con.setReadTimeout(15 * 1000);
				con.connect();
				// read the output from the server
				return new BufferedReader(new InputStreamReader(con.getInputStream()));
			} catch (IOException e) {
				LOG.debug("Try [{}] : failed to submit SPARQL query [{}]", tries, pquery, e);

				try {
					// wait 5 seconds and try again
					Thread.sleep(5000);
				} catch (InterruptedException ex) {
					LOG.error("Thread interrupted ", ex);
				}

				--tries;
			}
		}

    	return null;
	}

	/**
	 * Queries EDP with a single language code, two uppercase chars or empty,
	 * and returns a list of the catalogs with that language code
	 * (or without language code, resp.).
	 * The dct:spatial property of the catalogs is relevant.
	 * Typically a handful of catalogs is returned.
	 * @param langCode Language code; two Uppercase chars (GB,DE,...); __ or EU for pan-European.
	 * @return List of catalog URIs pertaining ot langCode.
	 */
	private ArrayList<String> getCatalogURIs (String langCode) throws LanguageFailedException {
		ArrayList<String> res = new ArrayList<>();

		// EDP catalogs without dct:spatial property
		// (being bound to SPARQL variable ?c when existing)
		// are pan-European:
		String spatial = ("__".equals(langCode)) ?
				"!bound(?c)" :
				"str(?c)=\"" + langCode + "\"";

		String query = "select ?s where {?s a dcat:Catalog. optional {?s <http://purl.org/dc/terms/spatial> ?c}. filter (" + spatial + "). }";

		try (BufferedReader reader = issueEdpSparqlQuery(query)) {
			String line;
			if (reader != null) {
				while ((line = reader.readLine()) != null) {
					//~ System.out.println(line.substring(1, line.length()-1));
					if (line.lastIndexOf(':', line.length() - 2) >= 0)    // keep out header line
						res.add(line.substring(1, line.length() - 1));    // trim quotation marks
				}
				LOG.debug("Fetched [{}] catalogues for language [{}]", res.size(), langCode);
			} else {
				throw new LanguageFailedException(langCode);
			}
		} catch (IOException e) {
			LOG.error("Failed to get catalog URIs for language [{}]", langCode, e);
			throw new LanguageFailedException(langCode);
		}

		return res;
	}

	/**
	 * Number of buckets for TLSH.
	 * Originally 256; reduced to account for shorter strings.
	 */
	private static final int NBuckets = 64;
	
	private int[] bucketCount = new int[NBuckets], tmpaux = new int[NBuckets];

	/**
	 * computes TLSH fingerprint of string ps (which should not be too short)
	 * and stores it as values 0..3 in int[NBuckets] fingerprint.
	 * Not reentrant in same class instance, due to use of class variables.
	 */
	private void TLSHfingerprint (String ps, int[] fingerprint) throws IllegalArgumentException {
		int i;
		
		//reset counters:
		for (i = 0; i < NBuckets; i++)
			bucketCount [i] = 0;

		//initialize sliding 5-char window:
		int c0 = (int)ps.charAt(0), c1 = (int)ps.charAt(1), c2 = (int)ps.charAt(2),
			c3 = (int)ps.charAt(3), c4 = (int)ps.charAt(4);

		i = 5;

		while (true) {
			bucketCount [pearson3b (c0, c1, c2)]++;
			bucketCount [pearson3b (c0, c3, c1)]++;
			bucketCount [pearson3b (c1, c0, c4)]++;
			bucketCount [pearson3b (c3, c2, c0)]++;
			bucketCount [pearson3b (c4, c0, c2)]++;
			bucketCount [pearson3b (c3, c4, c0)]++;

			//exit when end of string reached:
			if (i >= ps.length())
				break;

			// slide window forward:
			c0 = c1; c1 = c2; c2 = c3; c3 = c4; c4 = (int)ps.charAt(i);
			i++;
		}

		// do remaining 4 triples:
		bucketCount [pearson3b (c1, c2, c3)]++;
		bucketCount [pearson3b (c1, c4, c2)]++;
		bucketCount [pearson3b (c4, c3, c1)]++;
		bucketCount [pearson3b (c2, c3, c4)]++;

		//sort bucketCount, get quartils:
		System.arraycopy(bucketCount, 0, tmpaux, 0, bucketCount.length);
		Arrays.sort (tmpaux);
			
		int qutil_1 = tmpaux [(tmpaux.length)/4],
		qutil_2 = tmpaux [(tmpaux.length)/2],
		qutil_3 = tmpaux [(3*tmpaux.length)/4];

		if (qutil_1 < 0 || qutil_1 > qutil_2 || qutil_2 > qutil_3)
			throw new IllegalArgumentException("Failed to generate TLSH fingerprint. Quartiles inconsistent.");

		//encode :
		for (i = 0; i < NBuckets; i++) {
			int bi = bucketCount [i];
			// ensure 0→0:
			fingerprint[i] = (bi <= qutil_1? 0: bi >= qutil_3? 3 : bi >= qutil_2? 2 : 1);
		}
	}

	/**
	 * Pearson's hash function of three bytes.
	 * @return Hash value in range 0..NBuckets - 1.
	 */
	private int pearson3b(int pc0, int pc1, int pc2) {
		return (pearsonTable[pearsonTable[pc0&255] ^ (pc1&255)] ^ (pc2&255)) % NBuckets;
	}

    /**
     * English stop words, those to be removed from strings before fingerprinting.
     * Only very few, in order not to destroy relevant semantics.
     */
    private static final HashSet<String> EnglishStopWordsL1 = new HashSet<> (Arrays.asList (
			"it", "there", "if", "of",
			// conjunctions:
			"and", "so", "yet", "or", "moreover", "also", "too", "thus", "hence", "therefore", "furthermore", "likewise",
			// determiners:
			"a", "an", "the", "other", "another", "some", "any", "its", "their", "such",
			"all", "every", "each",    // but retain one, same, many and most
			//verbs:
			"is", "are", "be", "was", "were", "been", "do", "does", "did", "will", "would",
			// but retain can…, may…, shall…, must
			// foreign:
			"la", "der", "y", "de"));


	/**
	 * "Random" permutation of bytes, for use in Pearson's hash function.
	 */
	private final static int[] pearsonTable = {
		98,  6, 85,150, 36, 23,112,164,135,207,169,  5, 26, 64,165,219, //  1
		61, 20, 68, 89,130, 63, 52,102, 24,229,132,245, 80,216,195,115, //  2
		90,168,156,203,177,120,  2,190,188,  7,100,185,174,243,162, 10, //  3
		237, 18,253,225,  8,208,172,244,255,126,101, 79,145,235,228,121, //  4
		123,251, 67,250,161,  0,107, 97,241,111,181, 82,249, 33, 69, 55, //  5
		59,153, 29,  9,213,167, 84, 93, 30, 46, 94, 75,151,114, 73,222, //  6
		197, 96,210, 45, 16,227,248,202, 51,152,252,125, 81,206,215,186, //  7
		39,158,178,187,131,136,  1, 49, 50, 17,141, 91, 47,129, 60, 99, //  8
		154, 35, 86,171,105, 34, 38,200,147, 58, 77,118,173,246, 76,254, //  9
		133,232,196,144,198,124, 53,  4,108, 74,223,234,134,230,157,139, // 10
		189,205,199,128,176, 19,211,236,127,192,231, 70,233, 88,146, 44, // 11
		183,201, 22, 83, 13,214,116,109,159, 32, 95,226,140,220, 57, 12, // 12
		221, 31,209,182,143, 92,149,184,148, 62,113, 65, 37, 27,106,166, // 13
		  3, 14,204, 72, 21, 41, 56, 66, 28,193, 40,217, 25, 54,179,117, // 14
		238, 87,240,155,180,170,242,212,191,163, 78,218,137,194,175,110, // 15
		43,119,224, 71,122,142, 42,160,104, 48,247,103, 15, 11,138,239  // 16
	};


}
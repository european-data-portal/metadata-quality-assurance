package de.fhg.fokus.edp.mqa.service.ckan;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfig;
import de.fhg.fokus.edp.mqa.service.config.MqaServiceConfigKeys;
import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.model.Violation;
import de.fhg.fokus.edp.mqa.service.validation.ValidationData;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClient;
import de.fhg.fokus.edp.mqa.service.validation.ValidationDataClientType;
import de.fhg.fokus.edp.mqa.service.validation.ViolationParser;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by bdi on 02/06/15.
 */
@ApplicationScoped
public class ViolationParserJsonImpl implements Serializable, ViolationParser {

    @Inject
    @Log
    private Logger LOG;
    @Inject
    @ValidationData(ValidationDataClientType.PERSISTENCE)
    private ValidationDataClient vdc;

    @Inject
    @MqaServiceConfig(MqaServiceConfigKeys.VALIDATION_SCHEMA_JSON_URL)
    private String schemaUrl;
    private JsonNode schema;

    @PostConstruct
    public void init() {
        try {
            schema = JsonLoader.fromURL(new URL(schemaUrl));
            LOG.debug("Loaded JSON schema from {}", schemaUrl);
        } catch (IOException e) {
            LOG.error("Cannot load JSON schema from {}", schemaUrl);
        }
    }

    @Override
    public Set<Violation> validate(JsonNode jsonNode) {

        Set<Violation> violations = new HashSet<>();
        JsonSchemaFactory jsf = JsonSchemaFactory.byDefault();

        JsonSchema js;
        try {
            js = jsf.getJsonSchema(schema);
            boolean DEEP_CHECK = true;
            ProcessingReport pr = js.validate(jsonNode, DEEP_CHECK);
            if (!pr.isSuccess()) {
                for (ProcessingMessage aPr : pr) {
                    JsonNode violationJson = aPr.asJson();
                    if (violationJson.get("instance") != null) {
                        String name = violationJson.get("instance").get("pointer").asText();
                        String instance = violationJson.get("message").asText();
                        Violation violation = vdc.createViolation();
                        violation.setViolationInstance(instance);
                        violation.setViolationName(name);
                        violations.add(violation);
                    }
                }
            }

        } catch (ProcessingException | NullPointerException e) {
            LOG.error("Cannot process json from dataset: {}", jsonNode.toString(), e);
        }

        return violations;
    }
}

package de.fhg.fokus.edp.mqa.service.timer;

import de.fhg.fokus.edp.mqa.service.log.Log;
import de.fhg.fokus.edp.mqa.service.report.ReportHandler;
import org.jboss.weld.context.bound.BoundRequestContext;
import org.jboss.weld.context.bound.BoundSessionContext;
import org.quartz.*;
import org.slf4j.Logger;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bdi on 03/04/16.
 */
@DisallowConcurrentExecution
public class ReportGenerationJob implements InterruptableJob {

    /**
     * The Report handler.
     */
    @Inject
    ReportHandler reportHandler;
    @Inject
    @Log
    private Logger LOG;
    private Thread thread;

    @Inject
    private MqaScheduler mqaScheduler;

    @Override
    public void interrupt() throws UnableToInterruptJobException {
        thread.interrupt();
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        thread = Thread.currentThread();
        // Create session and request manually
        BoundRequestContext requestContext = CDI.current().select(BoundRequestContext.class).get();
        BoundSessionContext sessionContext = CDI.current().select(BoundSessionContext.class).get();
        Map<String, Object> requestMap = new HashMap<>();
        Map<String, Object> sessionMap = new HashMap<>();
        try {
            sessionContext.associate(sessionMap);
            sessionContext.activate();
            requestContext.associate(requestMap);
            requestContext.activate();
            if (mqaScheduler.getPreviousOrNextValidationRunDate() != null) {
                reportHandler.generateReports();
            }
        } finally {
            requestContext.invalidate();
            requestContext.deactivate();
            requestContext.dissociate(requestMap);
            sessionContext.invalidate();
            sessionContext.deactivate();
            sessionContext.dissociate(sessionMap);
        }
    }
}

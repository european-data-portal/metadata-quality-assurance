package de.fhg.fokus.edp.mqa.service.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by bdi on 05/10/15.
 */
public interface DatasetSimilarity extends Serializable {

    /**
     * Gets id.
     *
     * @return the id
     */
    Long getId();

    /**
     * Gets dataset instance id a.
     *
     * @return the dataset instance id a
     */
    String getDatasetInstanceIdA();

    /**
     * Sets dataset instance id a.
     *
     * @param datasetInstanceIdA the dataset instance id a
     */
    void setDatasetInstanceIdA(String datasetInstanceIdA);

    /**
     * Gets dataset instance id b.
     *
     * @return the dataset instance id b
     */
    String getDatasetInstanceIdB();

    /**
     * Sets dataset instance id b.
     *
     * @param datasetInstanceIdB the dataset instance id b
     */
    void setDatasetInstanceIdB(String datasetInstanceIdB);

    /**
     * Gets score.
     *
     * @return the score
     */
    double getScore();

    /**
     * Sets score.
     *
     * @param score the score
     */
    void setScore(double score);

    /**
     * Gets creation date.
     *
     * @return the creation date
     */
    LocalDateTime getCreationDate();

    /**
     * Sets creation date.
     *
     * @param creationDate the creation date
     */
    void setCreationDate(LocalDateTime creationDate);

    /**
     * Gets db update.
     *
     * @return the db update
     */
    LocalDateTime getDbUpdate();

    /**
     * Sets db update.
     *
     * @param dbUpdate the db update
     */
    void setDbUpdate(LocalDateTime dbUpdate);
}

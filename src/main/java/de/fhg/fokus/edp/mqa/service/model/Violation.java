package de.fhg.fokus.edp.mqa.service.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by bdi on 16/04/15.
 */
public interface Violation extends Serializable {

    /**
     * Gets id.
     *
     * @return the id
     */
    long getId();

    /**
     * Gets violation name.
     *
     * @return the violation name
     */
    String getViolationName();

    /**
     * Sets violation name.
     *
     * @param violationName the violation name
     */
    void setViolationName(String violationName);

    /**
     * Gets violation instance.
     *
     * @return the violation instance
     */
    String getViolationInstance();

    /**
     * Sets violation instance.
     *
     * @param violationInstance the violation instance
     */
    void setViolationInstance(String violationInstance);

    /**
     * Gets db update.
     *
     * @return the db update
     */
    LocalDateTime getDbUpdate();

    /**
     * Sets db update.
     *
     * @param dbUpdate the db update
     */
    void setDbUpdate(LocalDateTime dbUpdate);
}
